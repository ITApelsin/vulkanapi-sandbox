# Vulkan API

## Обзор

Vulkan приложение начинается с настройки Vulkan API через `VkInstance`, который определяет наименование приложения и используемые расширения.

Затем запрашивается и выбирается одно или несколько hardware-оборудование (`VkPhisicalDevices`) для выполнения операций. 

Далее создаются логические устройства (`VkDevice`) для которых указываются функции и семьи очередей, которые они будут использовать. 

Для отображения результатов отрисовки в окно операционной системы необходимо 2 компонента: `VkSurfaceKHR` и `VkSwapchainKHR`

> Постфикс KHR означает, что данный объект - расширение Vulkan API. 

Для отображения отрисованного изображения из swap chain его нужно завернуть в `VkImageView` или `VkFramebuffer`.

Графический конвейер `VkPipeline` устанавливает состояние графического оборудования: viewport, depth-buffer и т.д. Программное состояние определяется `VkShaderModule`, который создается из байт-кода шейдера. Графическому конвейеру необходимо знать с какими буферами он работает, которые назначаются через `VkRenderPass`.`VkRenderPass` определяет привязки, под-проходы, взаимосвязи и устанавливает как привязки используются в проходах отрисовки.

> Одно из основных отличий Vulkan API от существующих графических API - почти вся конфигурация графического конвейера устанавливается заранее. Если понадобится внести изменения в этапы отрисовки, графический конвейер потребуется пересоздать. Это так же означает, что все состояние должно быть описано явно. Значений по умолчанию нет.
>
> На практике создаются множество экземпляров `VkPipeline` заранее для всех отличных проходов.
>
> > Поскольку используется аналог компиляции с опережением в отличие от компиляции "точно в срок" у драйвера больше возможностей для оптимизации, а производительность во время выполнения более предсказуема т.к. большие изменения состояния граф. конвейера четко выражены.

Большинство операций Vulkan выполняются асинхронно через отправку в `VkQueue`.  Очереди выделяются из семейства очередей. Семейства очередей поддерживают определенный набор операций.

Перед тем как операции попадут в очередь они записываются в `VkCommandBuffer`. Выделяет эти буферы `VkCommandPool`, который связан с определенной очередью команд. Буфер команд записывается для каждого возможного изображения, а в момент отрисовки выбирается нужный.

Цикл отрисовки: 

- Запросить изображение из swap chain при помощи `vkAcquireImageKHR`
- Выбрать нужный буфер команд и выполнить его при помощи `vkQueueSubmit`
- Вернуть изображение в swap chain для отображения на экране при помощи `vkQueuePresentKHR`

> Операции, отправленные в очередь, выполняются асинхронно. Поэтому необходимо использовать объекты синхронизации.

Объекты Vulkan создаются при помощи функций группы `vkCreateXXX` или выделяются при помощи `vkAllocateXXX` и уничтожаются/высвобождаются при помощи `vkDestroyXXX` и `vkFreeXXX`. Все функции имеют необязательный аргумент `pAllocator` при помощи которого можно указать ссылку на пользовательскую функцию работы с памятью.

Vulkan отлично работает и без создания окна если речь идет о GPGPU или off-screen rendering.

Почти все функции Vulkan возвращают `VkResult`, который принимает значение `VK_SUCCESS` или код ошибки.

Обобщая (простая программа):

1. Создать `VkInstance`
2. Выбрать совместимое графическое оборудование `VkPhysicalDevice`
3. Создать логическое устройство `VkDevice` и очередь команд `VkQueue`
4. Создать window, window surface и swap chain.
5. Обернуть swap chain изображения в `VkImageView`.
6. Создать `VkRenderPass`, в котором определить цели рендера и их использование
7. Создать framebuffer для render pass
8. Настроить `VkPipeline` 
9. Выделить и настроить командный буфер с командами отрисовки для каждого swap chain изображения.
10. Отрендерить кадр путем захвата изображения, исполнения буфера команд и возвращения изображения в пул для отображения.

## Instance

`VkInstance` инициализирует библиотеку, устанавливает ее связь с приложением и сообщает драйверу некоторые подробности о программе.

```c++
VkApplicationInfo appInfo{};
appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
appInfo.pApplicationName = "Hello Triangle";
appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
appInfo.pEngineName = "No Engine";
appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
appInfo.apiVersion = VK_API_VERSION_1_0;
```

Следующая обязательная структура сообщает драйверу какие глобальные расширения и уровни валидации намеревается использовать приложение. Слово "Глобальные" означает, что они применяются на все приложение и не характерны для оборудования.

> Необходимые расширения для создания окна можно запросить у самого GLFW

```c++
uint32_t glfwExtensionCount = 0;
const char** glfwExtensions;
glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

VkInstanceCreateInfo createInfo{};
createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
createInfo.pApplicationInfo = &appInfo;
createInfo.enabledExtensionCount = glfwExtensionCount;
createInfo.ppEnabledExtensionNames = glfwExtensions;
createInfo.enabledLayerCount = 0; // do not use validation layers
```

Затем создаем экземпляр `VkInstance`

```c++
VkResult result = vkCreateInstance(&createInfo, nullptr, &instance);
```

`vkCreateInstance` может вернуть ошибку `VK_ERROR_EXTENSION_NOT_PRESENT` если запрошенное расширение недоступно. 

Для вывода списка всех доступных расширений: 

```c++
uint32_t extensionCount = 0;
vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
std::vector<VkExtensionProperties> extensions(extensionCount);
vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());

std::cout << "available extensions:\n";
for (const auto& extension : extensions) {
    std::cout << '\t' << extension.extensionName << '\n';
}
```

## Уровни валидации

В основу дизайна Vulkan API положен минимализм. Это означает крайнюю ограниченность проверок ошибок по умолчанию. Поэтому легко получить критическую ошибку или неопределенное выполнение. Однако проверки могут быть добавлены при помощи *уровней валидации*.  

**Уровни валидации** - необязательные компоненты, которые работают по принципу оберток реальных функций API. Например: 

```c++
VkResult vkCreateInstance(const VkInstanceCreateInfo* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkInstance* instance) {
    if (pCreateInfo == nullptr || instance == nullptr) {
        log("Null pointer passed to required parameter!");
        return VK_ERROR_INITIALIZATION_FAILED;
    }
    return real_vkCreateInstance(pCreateInfo, pAllocator, instance);
}
```

Уровни валидации позволяют: 

- проверять аргументы вызовов на соответствие спецификации
- отслеживать создание и уничтожение объектов для обнаружения утечки памяти
- проверять потокобезопасности вызовов 
- логировать все вызовы api
- осуществлять трекинг вызовов для профилирования и повтора

Уровни валидации могут быть использованы на тех системах, на которых они установлены. 

Формально существуют 2 типа уровней валидации: instance (проверяют общие вызовы) и device specific (проверяют характерные для GPU).

> Device specific - устаревшая функциональность. 

Как и в случае с расширениями уровни валидации подключаются путем перечисления их имен.

> Все стандартные проверки включены в уровень валидации Vulkan SDK `VK_LAYER_KHRONOS_validation`

```c++
const std::vector<const char*> validationLayers = {"VK_LAYER_KHRONOS_validation"};
createInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
createInfo.ppEnabledLayerNames = validationLayers.data();
```

## Физические устройства

Фактически можно выбрать любое количество видеокарт и использовать их одновременно. Выбранная видеокарта сохраняется в дескрипторе `VkPhysicalDevice`, который будет неявно уничтожен вместе с экземпляром `VkInstance`.  

Для получения списка совместимого оборудования:

```c++
uint32_t deviceCount = 0;
vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr);
if (deviceCount == 0) {
    throw std::runtime_error("failed to find GPUs with Vulkan support!");
}
std::vector<VkPhysicalDevice> devices(deviceCount);
vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data());
```

### Оценка пригодности видеокарты

Имя, тип и поддерживаемая версия Vulkan могу быть запрошены при помощи `vkGetPhysicalDeviceProperties.`

```c++
VkPhysicalDeviceProperties deviceProperties;
vkGetPhysicalDeviceProperties(device, &deviceProperties);
```

Поддержка опционального функционала, такого texture compression, 64 bit floats или multi viewport rendering (используется для VR) может быть запрошена при помощи `vkGetPhysicalDeviceFeatures`

```c++
VkPhysicalDeviceFeatures deviceFeatures;
vkGetPhysicalDeviceFeatures(device, &deviceFeatures);
```

> Пример (предикат "***дискретная карта поддерживающая шейдер геометрии***")
> ```c++
> bool isDescreteAndGeometryShaderSupport(VkPhysicalDive)
> {
> 	VkPhysicalDeviceProperties deviceProperties;
>     	VkPhysicalDeviceFeatures deviceFeatures;
>     	vkGetPhysicalDeviceProperties(device, &deviceProperties);
>     	vkGetPhysicalDeviceFeatures(device, &deviceFeatures);
>     
>     	return deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU 
>            && deviceFeatures.geometryShader;
> }
> ```

> Для выбор наиболее подходящей видеокарты можно "взвесить"  доступные видеокарты распределив веса по числу поддерживаемых функций и выбрать карту с наибольшим весом.

Как упоминалось ранее почти все операции в Vulkan помещаются в очереди. Очереди производятся *семействами очередей* и каждое семейство поддерживает определенный набор команд.

Необходимо проверить какие семейства очередей доступны и какие из них поддерживают необходимые команды. 

Пример:

```c++ 
#include <optional>
struct QueueFamilyIndices {
    std::optional<uint32_t> graphicsFamily;
    
    bool isComplete() {
        return graphicsFamily.isComplete();
    }
};
QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device) {
	QueueFamilyIndices indices;

    uint32_t queueFamilyCount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());
    
    int i = 0;
    for (const auto& queueFamily : queueFamilies) {
        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            indices.graphicsFamily = i;
            break;
        }
        // find any else queue families
        if (indices.isComplete()) {
            break;
        }
        i++;
    }
    
    return indices;
}
```

## Логическое устройство

После выбора физического устройства необходимо настроить логическое устройство для работы с ним. При создании логического устройства указываются возможности, которые мы хотим использовать через структуру `VkPhysicalDeviceFeatures`, а так же сколько очередей каких семейств создавать для логического устройства (при помощи структуры `VkDeviceQueueCreateInfo`). 

Драйвера позволяют создавать лишь небольшое число очередей в каждом семействе и на практике не потребности в нескольких очередях одного семейства т.к. можно создать командные буферы в разных потоках, а затем перенести их в основной и записать их в единственную очередь.

Vulkan позволяет назначать приоритет очереди для влияние на планировщик при выполнении буфера команд при помощи float числа с диапазоном допустимых значения [0.0, 1.0]. Это имеет смысл даже если речь идет о единственной очереди.

Так же `VkDeviceCreateInfo` структура содержит поля для настройки расширений и уровней валидации как в случае с `VkInstanceCreateInfo`. В данном случае они относятся только к данному логическому устройству. Например специфичное для логического устройства расширение `VK_KHR_swapchain` позволяет отображать изображения с устройства в окнах.

> Предыдущие реализации Vulkan различали уровни валидации логического устройства и глобального экземпляра, но сейчас это не так, что означает игнорирование современными реализациями полей `enabledLayerCount` и `ppEnabledLayerNames` структуры `VkDeviceCreateInfo` игнорируются. Однако их настройка является хорошей идеей, если идет речь об обратной совместимости.

```c++ 
QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
float queuePriority = 1.0f;

VkDeviceQueueCreateInfo queueCreateInfo{};
queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
queueCreateInfo.queueFamilyIndex = indices.graphicsFamily.value();
queueCreateInfo.queueCount = 1;
queueCreateInfo.pQueuePriorities = &queuePriority;

VkPhysicalDeviceFeatures deviceFeatures{};

VkDeviceCreateInfo createInfo{};
createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
createInfo.pQueueCreateInfos = &queueCreateInfo;
createInfo.queueCreateInfoCount = 1;
createInfo.pEnabledFeatures = &deviceFeatures;
createInfo.enabledExtensionCount = 0;
createInfo.enabledLayerCount = 0;
```

Затем создается экземпляр логического устройства при помощи вызова `vkCreateDevice`, логика работы которого аналогична вызову `vkCreateInstance`

```c++
if (vkCreateDevice(physicalDevice, &createInfo, nullptr, &device) != VK_SUCCESS) {
    throw std::runtime_error("failed to create logical device!");
}
```

Логическое устройство должно быть явно уничтожено. И т.к. оно не взаимодействует напрямую с `vkInstance` ссылка для уничтожения на последний ему не нужна.

```c++
vkDestroyDevice(device, nullptr);
```

### Очереди

Очереди автоматически создаются вместе с логическим устройством и уничтожаются вместе с ним. 

Для получения ссылки на очередь используется вызов `vkGetDeviceQueue`

```c++
vkGetDeviceQueue(device, indices.graphicsFamily.value(), 0, &graphicsQueue);
```

## Window Surface

> Окна необязательны для работы Vulkan

Vulkan - платформа независимый API. Для отображения результатов отрисовки в оконной системе необходимо использовать WSI (Window System Integration) расширения. Одно из них - `VK_KHR_surface` (instance level extension). Оно представляет `VkSurfaceKHR` объект, который является абстракцией области для отображения изображений. Следовательно для него необходимо платформо-зависимое расширение (в случае с Windows оно называется `VK_KHR_win32_surface`)

Инициализировать окно необходимо сразу после инициализации библиотеки, т.к. особенности оконной системы могут влиять на выбор физ. устройства. 

Пример инициализации win32 окна при использовании расширения `VK_KHR_win32_surface`:

```c++
VkWin32SurfaceCreateInfoKHR createInfo{};
createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
createInfo.hwnd = glfwGetWin32Window(window); // окно ОС для отображения
createInfo.hinstance = GetModuleHandle(nullptr); // процесс
if (vkCreateWin32SurfaceKHR(instance, &createInfo, nullptr, &surface) != VK_SUCCESS) {
    throw std::runtime_error("failed to create window surface!");
}
```

> Технически `vkCreateWin32SurfaceKHR` - функция расширения, но она настолько часто используется, что стандартный загрузчик загружает ее автоматически

Для уничтожения поверхности используется вызов `vkDestroySurfaceKHR`

```c++
vkDestroySurfaceKHR(instance, surface, nullptr);
```

> Уничтожить VkSurface необходимо до уничтожения VkInstance

Хотя реализации Vulkan поддерживают WSI это не означает, что каждое устройство поддерживает рендеринг в окна операционной системы. Представление является queue-specific feature, следовательно необходимо найти семейство очередей, поддерживающее это c помощью `vkGetPhysicalDeviceSurfaceSupportKHR`.

```c++
struct QueueFamilyIndices {
    std::optional<uint32_t> graphicsFamily;
    std::optional<uint32_t> presentFamily;
    bool isComplete() {
        return graphicsFamily.has_value() & presentFamily.has_value();
    }
};
...
VkBool32 presentSupport = false;
vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &presentSupport);
if (presentSupport) {
    indices.presentFamily = i;
}
```

> Скорее всего семейство очереди отображения семейство очереди визуализации, но не обязательно.

## Swap chain

В Vulkan нет понятия *стандартный буфер кадра*, поэтому необходима инфраструктура управляющая буферами в которые осуществляется рендер до их визуализации на экране монитора - ***swap chain*** (цепочка подкачки).

Swap chain представляет собой набор изображений, ожидающих визуализации. Приложение запрашивает изображение, осуществляет рендер и возвращает его в очередь. Главная задача swap chain - синхронизация представления изображений с частотой обновления экрана.

`VK_KHR_swapchain` - расширение уровня устройства. Дополним проверку применимости устройства:

```c++
const std::vector<const char*> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
uint32_t extensionCount;
vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);
std::vector<VkExtensionProperties> availableExtensions(extensionCount);
vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtension.data());
std::set<std::string> requiredExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
for (const auto& extension : availableExtensions) {
    requiredExtensions.erase(extension.extensionName);
}
return requiredExtensions.empty();
```

> Обычно поддержка расширения `VK_KHR_surface` свидетельствует и поддержке расширения `VK_KHR_swapchain`, но проверить лишним не будет.

Теперь активируем данное расширение передав список требуемый расширений в структуре `VkDeviceCreateInfo`

```c++
createInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
createInfo.ppEnabledExtensionNames = deviceExtensions.data();
```

Простой проверки поддержки расширения недостаточно т.к. полученная swap chain может быть несовместима с созданной поверхностью.

Следующие 3 параметра необходимо проверить:

- базовая совместимость с поверхностью (min/max число изображений в цепочке подкачки, min/max высота и ширина этих изображений)
- формат изображений (pixel формат, цветовое пространство)
- доступные режимы представления 

```c++
struct SwapChainSupportDetails {
  	VkSurfaceCapabilitiesKHR capabilities;
   	std::vector<VkSurfaceFormatKHR> formats;
    std::vector<VkPresentModeKHR> presentModes;
};

SwapChainSupportDetails querySwapChainSupport(VkPhysicalDevice device, VkSurfaceKHR surface)
{
    SwapChainSupportDetails details;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, surface, details.capabilites);
    uint32_t formatCount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, nullptr);
    if (formatCount != 0) {
        details.formats,resize(formatCount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &formatCount, details.formats.data());
    }
    uint32_t presentModeCount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presentModeCount, nullptr);
    if (presentModeCount != 0) {
        details.presentModes.resize(presentModeCount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, surface, &presetModeCount, details.presentModes.data());
    }
    return details;
}
```

Расширим функцию определения пригодности физического устройства:

```c++
bool swapChainAdequate = false;
if (extensionsSupported) {
 	SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device, surface);
    swapChainAdequate = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
}
```

Каждая запись `VkSurfaceFormatKHR` содержит поля `format` и `colorSpace`. 

- `format` определяет цветовые каналы и их типы, к примеру: `VK_FORMAT_B8G8R8A8_SRGB` означает формат из 4 каналов (Blue, Green, Red + Alpha) в указанном порядке с типом 8 bit unsigned integer т.е. 32 bit per pixel.

- `colorSpace` определяет поддерживается ли SRGB цветовое пространство или нет, используя флаг `VK_COLOR_SPACE_SRGB_NONLINEAR_KHR` 

  > В старых версиях данный флаг назывался `VK_COLORSPACE_SRGB_NONLINEAR_KHR` 

Следует по возможности использовать цветовое пространство SRGB т.к. это приводит к более точной передачи цвета к томуже это современный стандарт для изображений, например текстур. Самым распространенным форматом SRGB является: `VK_FORMAT_B8G8R8A8_SRGB`

> Если SRGB формат не будет найден то можно проранжировать другие формат по их пригодности, но в большинстве достаточно выбрать первый из предложенных.

```c++
VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
    
	for (const auto& availableFormat : availableFormats) 
	{
    	if (availableFormat.format == VK_FORMAT_B8G8R8A8_SRGB && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
        	return availableFormat;
    	}
	}
    return availableFormats[0];
}
```

**Режим представления** вероятно самая важная настройка для swap chain т.к. она устанавливает условия для фактического отображения изображений на экране монитора. В Vulkan API доступно 4 режима представления:

- `VK_PRESENT_MODE_IMMEDIATE_KHR` - изображения возвращенные в очередь приложением незамедлительно отображаются на экране монитора.

  > Может привести к разрывам изображения

- `VK_PRESENT_MODE_FIFO_KHR` - работает по принципу First-In-First-Out. Swap chain представляет собой очередь. Монитор при обновлении берет изображение из начала очереди, а программа вставляет новое изображение в конец очереди. Если очередь переполнена, программа ждет. Данный режим в наибольшей степени соответствует вертикальной синхронизации из современных игр. Момент обновления дисплея известен как "*vertical blank*"

- `VK_PRESENT_MODE_FIFO_RELAXED_KHR` - отличается от предыдущего режима тем, что если приложение опоздало и очередь пуста из-за предыдущего vertical blank изображение отображается незамедлительно

  > Может привести к разрывам изображения

- `VK_PRESENT_MODE_MAILBOX_KHR` - другая вариация режима `VK_PRESENT_MODE_FIFO_KHR`. Вместо блокировки приложения при переполнении очереди, новые изображения заменяют уже имеющиеся в очереди.

  > Данный режим используется для реализации тройной буферизации, которая позволяет избежать разрывов изображения со значительно меньшими задержками, чем при стандартной вертикальной синхронизации, использующей двойную буферизацию.

> Гарантированно доступен только режим `VK_PRESENT_MODE_FIFO_KHR`, однако тройная буферизация будет лучшим выбором если она доступна.

```c++
VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
    for (const auto& availablePresentMode : availablePresentModes) {
        if (availablePresentMode == VK_PRESENT_MODE_MAILBOX_KHR) {
            return availablePresentMode;
        }
    }
    
    return VK_PRESENT_MODE_FIFO_KHR;
}
```

**Swap extent** - это разрешение изображений в swap chain, которое почти всегда равно разрешению окна. Диапазон допустимых разрешений определен в структуре `VkSurfaceCapabilitiesKHR`.

Vulkan просит устанавливать разрешение изображений подкачки равным разрешению окна, значение высоты и ширина которого переданы в поле `currentExtent`. Однако некоторые оконные менеджеры позволяют устанавливать разрешения для изображений цепочки подкачки отличные от разрешения окна. В этом случае полям `width` и `height` поля `curentExtent` устанавливается специальное значение: максимальное значение для типа uint32_t. В этом случаем следует выбирать разрешение, которое наилучшим образом соответствует окну в пределах границ `minImageExtent` и `maxImageExtent`.

```c++
#include <cstdint> // Necessary for UINT32_MAX
#include <algorithm> // Necessary for std::min and std::max

VkExtent2D chooseSwapExtent(const VkSurfaceCapabilitiesKHR& capabilities) {
    if (capabilities.currentExtent.width != UINT32_MAX) {
        return capabilities.currentExtent;
    } else {
        VkExtent2D actualExtent = {WIDTH, HEIGHT};
        actualExtent.width = std::max(capabilities.minImageExtent.width, std::min(capabilities.maxImageExtent.width, actualExtent.width));
        actualExtent.height = std::max(capabilities.minImageExtent.height, std::min(capabilities.maxImageExtent.height, actualExtent.height));
        
        return actualExtent;
    }
}
```

При создании swap chain так же необходимо определить размеры создаваемой очереди. Минимальный объем цепочки подкачки предается в поле `VkSurfaceCapabilitiesKHR.minImageCount`. Однако использование минимума означает что иногда придется подождать пока драйвер завершит необходимые операции. Поэтому рекомендуется запросить по крайней мере на 1 изображение больше минимального, при этом следует учесть максимальный размер очереди (`VkSurfaceCapabilitiesKHR.maxImageCount`) для которого 0 - специальное значение означающее отсутствие ограничений на максимальный размер очереди.

Поле `imageArrayLayers` указывает число слоев, из которых состоит каждое изображение в цепочке. Всегда равно 1, если речь не идет о 3D стерео-приложении. 

`imageUsage` - битовое поле определяющее для каких операций будет использовано полученное изображение. Для рендера напрямую в предоставленное swap chain изображение нужно указать значение `VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT`. Однако рендер может быть осуществлен в другое "вне-экранное" изображение а затем перенесен в изображение цепочки (`VK_IMAGE_USAGE_TRANSFER_DST_BIT`).

Так же необходимо указать как swap chain-изображения будут использоваться разными семействами очередей (например в случае если для отображения и представления используются разные семейства: отрисовка изображения осуществляется в одной очереди, а затем передается в другую для отображения). Есть 2 режима управления очередями:

- `VK_SHARING_MODE_EXCLUSIVE` - изображение принадлежит одному семейству очередей и право владения должно быть передано явно перед использование в другом. 

  > Данный режим обеспечивает наилучшую производительность

- `VK_SHARING_MODE_CONCURRENT` - изображения могут использоваться разными семействами без правопередачи.

При помощи поля `preTransform` можно задать преобразование из поддерживаемых (например поворот на 90 градусов) для swap chain изображений.

Поле `compositeAlpha` указывает нужно ли использовать альфа-канал для смешивания с другими окнами в оконной системе. Значение `VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR` - игнорирование alpha канала.

Установка полю `clipped` значения `VK_TRUE` означает что нас не волнуют невидимые пиксели (например перекрытые другим окном). Обеспечивает наилучшую производительность.

`oldSwapChain` - ссылка на предшествующую swap chain. Используется при пересоздании цепочки, например если действительная оказывается непригодной (к примеру в следствии изменении размеров окна).

Создание swap chain:

```c++
void createSwapChain() {
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(physicalDevice);
    VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(swapChainSupport.formats);
    VkPresentModeKHR presentMode = chooseSwapPresentMode(swapChainSupport.presentModes);
    VkExtent2D extent = chooseSwapExtent(swapChainSupport.capabilities);
    uint32_t imageCount = swapChainSupport.capabilities.minImage + 1;
    if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount) {
        imageCount = swapChainSupport.capabilities.maxImageCount;
    }
    
    VkSwachainCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createInfo.surface = surface;
    createInfo.minImageCount = imageCount;
    createInfo.imageFormat = surfaceFormat.format;
    createInfo.imageColorSpace = surfaceFormat.colorSpace;
    createInfo.imageExtent = extent;
    createInfo.presentMode = presentMode;
    createInfo.imageArrayLayers = 1;
    createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
    createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    createInfo.presentMode = presentMode;
    createInfo.clipped = VK_TRUE;
    createInfo.oldSwapchain = VK_NULL_HANDLE;
    
    QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
    uint32_t queueFamilyIndices[] = {indices.graphicsFamily.value(), indices.presentFamily.value()};
    if (indices.graphicsFamily != indices.presentFamily) {
        createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createInfo.queueFamilyIndexCount = 2;
        createInfo.pQueueFamilyIndices = queueFamilyIndices;
    } else {
        createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createInfo.queueFamilyIndexCount = 0; // optional
        createInfo.pQueueFamilyIndices = nullptr; // optional
    }
    
    VkSwapchainKHR swapChain;
    if (vkCreateSwapchainKHR(device, &createInfo, nullptr, &swapChain) != VK_SUCCESS) {
        throw std::runtime_error("failed to create swap chain!");
    }
}
```

Объект VkSwapchainKHR должен быть явно уничтожен.

```c++
vkDestroySwapchainKHR(device, swapChain, nullptr);
```

Остается только получить изображения swap chain. Изображения swap chain будут автоматически уничтожены при уничтожения swap chain.

> Мы указываем запрашиваем минимальное число swap chain изображений. Реализация может создать больше. Поэтому сперва необходимо запросить действительное число изображений.

```c++
uint32_t imageCount;
std::vector<VkImage> swapChainImages;
vkGetSwapchainImagesKHR(device, swapChain, &imageCount, nullptr);
swapChainImages.resize(imageCount);
vkGetSwapChainImagesKHR(device, swapChain, &imageCount, swapChainImages.data());
```

> Выбранные extent и image format swap chain изображений понадобятся для других операций.

### Пересоздание swap chain

Window surface может измениться так, что swap chain больше не будет с ней совместима и ее нужно будет пересоздать.

```c++
void recreateSwapChain() {
    vkDeviceWaitIdle(device);
    
    createSwapChain();
    createImageViews();
    createRenderPass();
    createGraphicsPipeline();
    createFramebuffers();
    createCommandBuffers();
}
```

Сперва дождемся освобождения логического устройства т.к. используемые ресурсы не должны быть затронуты.

При пересоздании swap chain так же необходимо пересоздать следующие объекты, а старые экземпляры этих объектов удалить:

- *Image Views* т.к. они основаны на images из swap chain

- *Render Pass* т.к. он зависит от формата images из swap chain (несмотря на то, что изменение размера окна редко приводит к изменению формата image)

- *Graphics Pipeline* т.к. размер viewport и прямоугольник отсечения (scissor rectangle) настраиваются на этапе создания конвейера

  > Однако, чтобы не пересоздавать конвейер viewport и прямоугольник отсечения можно указать как динамическое состояние.

- *Framebuffers* т.к. они напрямую зависят от images из swap chain

- *Command Buffers* т.к. они зависят от фреймбуферов

> Создавать пул команд с нуля не нужно. Вместо этого можно уничтожить существующие буферы команд при помощи `vkFreeCommandBuffers` и использовать тот же пул для выделения новых. 
>
> ```c++
> vkFreeCommandBuffers(device, commandPool, static_cast<uint32_t>(commandBuffers.size()));
> ```

> Начать создание новой swap chain можно пока команды рисования для старой swap chain все еще находятся в конвейере (in-flight). Для этого старую swap chain необходимо указать в поле `oldSwapChain` в структуре `VkSwapchainCreateInfoKHR` и уничтожить ее после использования.

Vulkan сам сообщит, что swap chain больше не подходит для использования. Функции `vkAcquireNextImageKHR` и `vkQueuePresentKHR` **могут** возвращать следующие значения, указывающие на это:

- `VK_ERROR_OUT_OF_DATE_KHR`: swap chain стала несовместима с surface и **больше не может использоваться для рендеринга**. Обычно это происходит после изменения размера окна.
- `VK_SUBOPTIMAL_KHR`: swap chain не полностью соответствует surface, но **по-прежнему может использоваться для успешного отображения на экране**

```c++
VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);
if (VK_ERROR_OUT_OF_DATE_KHR == result) {
    recreateSwapChain();
    return;
} else if (VK_SUCCESS != result && VK_SUBOPTIMAL_KHR != result) {
    throw std::runtime_error("failed to acquire swap chain image!");
}
...
result = vkQueuePresentKHR(presentQueue, &presentInfo);
if (VK_ERROR_OUT_OF_DATE_KHR == result || VK_SUBOPTIMAL_KHR == result) {
    recreateSwapChain();
} else if (VK_SUCCESS != result) {
    throw std::runtime_error("failed to present swap chain image!");
}
currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
```

Хотя многие драйверы и платформы после изменения размера окна автоматически запускают `VK_ERROR_OUT_OF_DATE_KHR` **нет абсолютной уверенности** что это произойдет. Поэтому хорошей практикой будет ручная обработки изменения размеров окна. 

Для отслеживания изменений размеров окна подойдет флаг, который необходимо проверить в функции `drawFrame`. А для установки флага можно использовать функцию обратного вызова, установив ее при помощи`glfwSetFramebufferSizeCallback`

> Проверка флага должна осуществляться после вызова `vkQueuePresentKHR` чтобы семафоры находились в надлежащем состоянии. 

```c++
bool framebufferResized = false;

static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
   	frameBufferResized = true;
}

void initWindow() {
    ...
    glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
}

void drawFrame() {
    result = vkQueuePresentKHR(presentQueue, &presentInfo);
    if (VK_ERROR_OUT_OF_DATE_KHR == result || VK_SUBOPTIMAL_KHR == result || framebufferResized) {
        framebufferResized = false;
        recreateSwapChain();
    } else if (VK_SUCCESS != result) {
        ...
    }
}
```

### Обработка сворачивания окна

Swap chain может устареть и при сворачивании окна. Это особый случай изменения размеров окна, когда площадь фреймбуфера становится равна 0. 

В этом случае рендер можно просто остановить до тех пор, пока окно не будет развернуто вновь.

```c++
int width = 0, height = 0;
glfwGetFramebufferSize(window, &width, &height);
while(width == 0 || height == 0) {
    glfwGetFramebufferSize(window, &width, &height);
    glfwWaitEvents();
}
```

## Image views

Для использования `VkImage` включая те, что предоставляет swap chain в графическом конвейере необходимо создать объект `VkImageView`. Image View - это буквально представление изображения, описывающее принцип доступа к изображения, область изображения и прочее.

```c++
std::vector<VkImageView> swapChainImageViews;

void createImageViews() {
    swapChainImageViews.resize(swapChainImages.size());
    for (size_t i = 0; i < swapChainImages.size(); i++) {
        VkImageViewCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createInfo.image = swapChainImages[i];
        createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createInfo.format = swapChainImageFormat;
        
        createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        
        createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;
        
        if (vkCreateImageView(device, &createInfo, nullptr, &swapChainImageViews[i]) != VK_SUCCESS) {
            throw std::runtime_error("failed to create image views");
        }
    }
}
```

Параметр `VkImageViewCreateInfo`

- `viewType` указывает как интерпретировать изображение: 1D-, 2D-, 3D-текстуру или кубическую карту.

- `components` позволяет переключать цветовые каналы между собой. 

  > Например считывать значения для всех каналов из r компонента, получив тем самым монохромную текстуру
  >
  > Назначить 1 или 0 как константу для альфа канала и т.д.

- `subresourceRange` описывает какая часть изображения будет использована (слой, уровень детализации, назначение (аспект))

  > Аспекты изображения - цвет, глубина, трафарет, метаданные и т.д.

> Для стерео 3D изображения необходимо создать swap chain с несколькими уровнями и для каждого уровня создать ImageView представляющих правый и левый глаз.

Созданные `ImageView` объекты необходимо явно уничтожить при завершении программы:

```c++
for (auto imageView : swapChainImageViews) {
    vkDestroyImageView(device, imageView, nullptr);
}
```

ImageView достаточно для того, чтобы использовать изображение как текстуру, но для использования изображения как цель рендера необходим фреймбуфер.

## Графический конвейер

![Graphics pipeline](./img/graphics_pipeline.png)

Input assembler собирает сырые данные вершин из буферов. На данном этапе в том числе применяется индексный буфер, который избавляет от необходимости дублировать данные вершин. 

Vertex shader выполняется для каждой вершины и используется, в основном для трансформации из локальной системы координат в экранную и передачи данных о каждой вершине далее по конвейеру. 

Tessellation shader используется для разбиения (subdivide) геометрии на основе заданных правил для увеличения детализации геометрии. 

Geometry shader выполняется для каждого примитива (треугольник, линия, точка) и может отменить их отрисовку или сгенерировать новые примитивы. Похож на tessellation shader но более гибкий. 

> Не часто используется из-за недостаточной производительности большинства видеокарт за исключением встроенных графических процессоров Intel

На этапе растеризации (rasterization) каждый примитив раскладывается на фрагменты. Фрагменты - пиксельные элементы, заполняющие площадь примитивов во фреймбуфере. Все фрагменты, выходящие за пределы экрана и не проходящие тест глубины (depth testing), отсекаются, а атрибуты вершин интерполируются вдоль примитива.

Fragment shader выполняется для каждого фрагмента и определяет в какие фреймбуферы и с каким значением записываются фрагменты. Для этого используется интерполированные данные из vertex shader.

На этапе смешивания цветов (color blending) происходит смешивание фрагментов, относящихся к одному пикселю во фреймбуфере. Фрагменты могут перекрывать друг друга или смешиваться в зависимости от прозрачности. 

Этапы input assebler, rasterization и color blending непрограммируемые (fixed function stages). Их можно корректировать при помощи параметров, но принцип работы определен заранее. 

Остальные этапы программируются при помощи шейдеров - программ, одновременно выполняющихся на множестве GPU ядер для параллельной обработки вершин и фрагментов.

> Некоторые программируемые этапы можно пропустить.

В Vulkan почти все настройки графического конвейера задаются заранее, поэтому для переключение шейдера, привязки другого фреймбуфера или изменении функции смешивания, придется полностью пересоздать конвейер. Поэтому необходимо создать несколько конвейеров чтобы описать все комбинации   состояния.

### Shader modules

В отличие от более ранних графических API шейдеры должны быть переданы в Vulkan в виде байт-кода, который называется SPIR-V. 

> SPIR-V разработан для работы как с Vulkan так и с OpenCL

Преимущество байт-кода в том, что компиляция в native код GPU осуществляется значительно проще. Кроме того не допускается свободное трактование человеко-ориентированного синтаксиса, приводящего к различным результатам работы нетривиальных шейдеров на разных видеокартах.

Однако собственноручно писать SPIR-V байт-код не придется. Существуют платформо-независимые компиляторы, компилирующие GLSL в SPIR-V, которые можно использовать в том числе и в качестве библиотек для компиляции шейдеров во время исполнения. 

> Компилятор от Khronos `glslangValidator.exe` и компилятор от Google `glslc.exe` включены в Vulkan SDK.

### Vertex Shader

Вершинный шейдер обрабатывает каждую входящую вершину. В качестве входных данных он принимает вершинные атрибуты (координаты в мировом пространстве, цвет, нормаль, и текстурные координаты). Выходные данные - преобразованные координаты в пространстве отсечения (clip space) и атрибуты, которые передаются фрагментному шейдеру. Позже эти значения будут интерполированы для создания плавного градиента.

Координатами в пространстве отсечения называют четырехмерный вектор, принимаемый из вершинного шейдера, который преобразуется в нормализованные координаты устройства (normalized device coordinate) - это гомогенные координаты, находящиеся в диапазоне [-1, 1].

![normalized device coordinates](./img/normalized_device_coordinates.png)

> В отличие от OpenGL координата Y зеркально отражена, а  координаты Z находятся в диапазоне [0, 1]

> Можно отдавать непосредственно нормализованные координаты устройства, используя координаты пространства отсечения с единицей в качестве последнего компонента.

>  Как правило, нормализированные координаты хранятся в вершинном буфере.

```glsl
#version 450
vec2 positions[3] = vec2[] (
	vec2(0.0, -0.5),
    vec2(0.5, 0.5),
    vec2(-0.5, 0.5)
);

void main() {
    gl_Position = vec4(positionp[gl_VertexIndex], 0.0, 1.0);
}
```

### Fragment shader

Фрагментный шейдер выполняется для каждого фрагмента для определения его цвета, глубины и т.д. 

Для указания цвета в GLSL используются четырехкомпонентные RGBA-векторы с диапазоном [0, 1] для каждого канала. В fragment shader нет встроенных переменных для вывода цвета. Необходимо указать собственную выходную (`out`) переменную для каждого фреймбуфера, где модификатор `layout (location = 0)` указывает номер фреймбуфера. 

```glsl
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

void main() {
    outColor = vec4(1.0, 0.0, 0.0, 1.0);
}
```

Для интерполяция и передачи значений из вершинного шейдера во фрагментный используются пары выходных-входных (модификаторы `out` и `in`) параметров. Их имена в вершинном и фрагментном шейдерах могут отличаться, взаимосвязь устанавливается при помощи модификаторов `layout(location = <index>)` 

### Компиляция шейдеров

```shell
c:/VulkanSDK/x.x.x.x/Bin32/glslc.exe <input file> -o <output file>
```

> `libshaderc` - библиотека для компиляции кода GLSL в SPIR-V. Можно использовать для компиляции шейдеров во время выполнения.

### Shader module

Вспомогательная функция для загрузки бинарных данных в байт массив обернутый в `std::vector`. При открытии файла используется 2 флага:

- `ate` - установить указатель в конец файла для вычисления его размера
- `binary` - читать файл как двоичный

```c++
#include <fstream>
static std::vector<char> readFile(const std::string& filename) {
    std::ifstream file(filename, std::ios::ate | std::ios::binary);
    if (!file.is_open()) {
        throw std::runtime_error("failed to open file");
    }
    size_t fileSize = (size_t)file.tellg();
    std::vector<char> buffer(fileSize);
    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();
    return buffer;
}
```

Байт код шейдера необходимо обернуть в `VkShaderModule`. Для этого нужно передать указатель на буфер с байт-кодом и размер этого буфера через структуру `VkShaderModuleCreateInfo`. 

```c++
VkShaderModule createShaderModule(const std::vector<char>& code) {
    VkShaderModuleCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE)TYPE_SHADER_MODULE_CREATE_INFO;
    createInfo.codeSize = code.size();
    createInfo.pCode - reinterpret_cast<const uint32_t*>(code.data());
    
    VkShaderModule shaderModule;
    if (VK_SUCCESS != vkCreateShaderModule(device, &createInfo, nullptr, &shaderModule)) {
        throw std::runtime_error("failed to create shader module!");
    }
    return shaderModule;
}
```

> Тип указателя на буфер с байт-кодом шейдера - `uint32_t`. Следовательно буфер должен удовлетворять требованиям выравнивания `uint32_t`

Shader module всего лишь тонкая обертка для байт-кода шейдера. Компиляция и линковка байт-кода SPIR-V в машинный код не произойдет до тух пор, пока не будет создан графический конвейер. Это означает, что **шейдерные модули можно уничтожить после создания конвейера.**

```c++
vkDestroyShaderModule(device, shaderModule, nullptr);
```

Чтобы использовать шейдеры нужно подключить их к конкретным стадиям конвейера. Для этого используется структура `VkPipelineShaderStageCreateInfo`

```c++
VkPipelineShaderStageCreateInfo stageInfo{};
stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
stageInfo.stage = VK_SHADER_STAGE_VERTEX_BIT;
stageInfo.module = shaderModule;
stageInfo.pName = "main";
stageInfo.pSpecializationInfo = nullptr;
```

- `stage` - указывает программируемую стадию, к которой будет подключен шейдер. Для каждой программируемой стадии есть соответствующее значение из перечисления `VkShaderStageFlagBits`

- `module` - шейдерный модуль

- `pName` - имя entry point function. 

  > Один шейдерный модуль можно в различных сценариях, меняя поведение с помощью разных точек входа. 

- `pSpecializationInfo` - позволяет указать значение для шейдерных констант.

  > Поведение шейдера можно настраивать через значения для используемых констант. Этот способ более эффективных по сравнению с настройкой шейдера во время рендеринга т.к. компилятор может выполнять оптимизацию (например удалять операторы if, зависящие от этих значений)

### Непрограммируемые стадии конвейера

В отличие от ранних графических API, которые для большинства параметров графического конвейера были значения по умолчанию, **в Vulkan все состояния конвейера должны описываться явно**

Структура `VkPipelineVertexInputStateCreateInfo` описывает формат входных данных вершинного шейдера.

- описание атрибутов: тип данных, передаваемых в вершинный шейдер, привязка к буферу данных и смещение в нем.
- привязки: расстояния между элементами и то, каким образом связаны данные и выводимая геометрия (повершинная привязки или per-instnace)

```c++
VkPipelineVertexInputStateCreateInfo vertexInputInfo{};
vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
vertexInputInfo.vertexBindingDescriptionCount = 0;
vertexInputInfo.pVertexBindingDescriptions = nullptr;
vertexInputInfo.vertexxAttributeDescriptionCount = 0;
vertexInputInfo.pVertexAttributeDescriptions = nullptr;
```

Члены `pVertexBindingDescriptions` и `pVertexAttributeDescriptions` - указатели на массивы структур, описывающие вышеупомянутые данные.

> Вернемся к данному разделу позже

### Input assembler

Структура `VkPipelineInputAssemblyStateCreateInfo` описывает какая геометрия образуется из вершин и разрешен ли перезапуск для таких примитивов как line strip и triangle strip. Тип примитива указывается через поле `topology`, которое может принимать следующие значения:

- `VK_PRIMITIVE_TOPOLOGY_POINT_LIST` - каждая вершина отдельная точка
- `VK_PRIMITIVE_TOPOLOGY_LINE_LIST` - каждая пара вершин образует отрезок
- `VK_PRIMITIVE_TOPOLOGY_LINE_STRIP` - непрерывная ломанная. Каждая последующая вершина добавляет один отрезок (*возможен перезапуск примитива*)
- `VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST` - независимые треугольники
- `VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP` - цепь связанных треугольников, причем **две последние вершины предыдущего треугольника используются в качестве двух первых вершин следующего**. (*возможен перезапуск примитива*)

Для перезапуска примитива используется специальное значение `0xFFFF` или `0xFFFF_FFFF`. Для включения перезапуска примитива полю `primitiveRestartEnable` задать `VK_TRUE`

```c++
VkPipelineInputAssemblyStateCreateInfo inputAssembly{};
inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
inputAssembly.primitiveRestartEnable = VK_FALSE;
```

### Viewports and scissors

`VkViewport` описывает область фрймбуфера в которую осуществляется рендеринг.

> Почти всегда для viewport'a задаются координаты от `(0, 0)` до `(width, height)`
>
> Размеры SwapchainImages могут могут отличаться от размеров окна. Т.к. эти изображения будут использоваться в качестве фреймбуферов необходимо использовать **именно их** размер.

Поля `minDepth` и `maxDepth` определяют диапазон значений глубины фреймбуфера. Эти значения должны находится в диапазоне `[0.0f, 1.0f]`, однако допускается `minDepth > maxDepth`

```c++
VkViewport viewport{};
viewport.x = 0.0f;
viewport.y = 0.0f;
viewport.width = (float) swapChainExtent.width;
viewport.height = (float) swapChainExtent.height;
viewport.minDepth = 0.0f;
viewport.maxDepth = 1.0f;
```

Viewport определяет как изображение будет растянуто во фреймбуфере, а **scissors** определяет какие пиксели будут сохранены. *Все пиксели за пределами прямоугольника отсечения (scissor rectangle) будут отброшены на этапе растеризации*.

```c++
VkRect2D scissor{};
scissor.offset = {0, 0};
scissor.extent = swapChainExtent;
```

Информацию о viewport и scissor необходимо объединить в структуру `VkPipelineViewportStateCreateInfo`.

> На некоторых видеокартах можно использовать одновременно несколько viewport и прямоугольников отсечения поэтому информация о них передается в виде массива. Для использования нескольких viewport и scissor нужно включить соответствующую опцию GPU.

```c++
VkPipelineViewportStateCreateInfo viewportState{};
viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
viewportState.viewportCount = 1;
viewportState.pViewports = &viewport;
viewportState.scissorCount = 1;
viewportState.pScissors = &scissor;
```

### Растеризатор

Растеризатор преобразует геометрию, полученную из вершинного шейдера во множество фрагментов. На этом этапе выполняется *тест глубины (depth test)*, *отбраковка задней стороны (face culling)*, *тест отсечения (scissor test*). Необходимо настроить способ заполнения полигона фрагментами: заполнение всего полигона или только ребра (wireframe rendering). 

Все это настраивается в структуре `VkPipelineRasterizationStateCreateInfo`

```c++
VkPipelineRasterizationStateCreateInfo rasterizer{};
rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
rasterizer.depthClampEnable = VK_FALSE;
rasterizer.rasterizerDiscardEnable = VK_FALSE;
rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
rasterizer.lineWidth = 1.0f;
rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
rasterizer.depthBiasEnable = VK_FALSE;
rasterizer.depthBiasConstantFactor = 0.0f; // Optional
rasterizer.depthBiasClamp = 0; // Optional
rasterizer.depthBiasSlopeFactor = 0.0f; // Optional
```

- `depthClampEnable` - если установить `VK_TRUE`, то фрагменты, находящиеся за пределами ближней и дальней плоскости (не прошедшие тест глубины), не отсекаются, пододвигаются к ним. 

  >  Может пригодиться, например, для создания карты теней
  >
  >  Для использования нужно включить соответствующую опцию GPU

- `rasterizerDiscardEnable` - если `VK_TRUE`, то **стадия растеризации отключается**, а выходные данные не передаются во фреймбуфер.

- `polygonMode` определяет принцип заполнения примитива фрагментами:

  1. `VK_POLYGON_MODE_FILL` - полигоны полностью заполняются фрагментами
  2. `VK_POLYGON_MODE_LINE` - ребра полигонов преобразуются в отрезки
  3. `VK_POLYGON_MODE_POINT` - вершины полигонов рендерятся в виде точек
  
  > Для использования режимов кроме `VK_POLYGON_MODE_FILL` необходимо включить соответствующую опцию GPU.
  
- `lineWidth` - толщина отрезков. 

  > Максимальная ширина отрезков зависит от оборудования. 
  >
  > Для отрезков толще `1.0f` требуется включить опцию GPU `wideLines`

- `cullMode` - тип отсечения (face culling). Можно либо совсем отключить отсечение, либо включить отсечение лицевых и/ или не лицевых граней.

- `frontFace` - порядок обхода вершин для определения лицевых граней (по часовой стрелке или против)

> Растеризатор может изменить значение глубины фрагмента, добавив постоянное значение или сместив глубину в зависимости от наклона. (см. поля `depthBias**`)

### Мультисемплинг

Multisample anti-aliasing (MSAA) - один из способов сглаживания. 

Обычно цвет пикселя определяется на основе 1 sample point, которая в общем случае находится в центре целевого пикселя. Если ребро объекта проходит через целевой пиксель, но не перекрывает sample point, данный пиксел останется пустым, что и приводит к эффекту зубчатой лестницы на краях объекта, известному как *aliasing*.

 ![aliasing](./img/aliasing.jpg)

В случае с MSAA используется несколько sample points для определения цвета пикселя. ***Чем больше sample points тем лучше результат но сложнее вычисления.***

![MSAA](./img/msaa.jpg)

 Основное преимущество мультисэмплинга в том, что **фрагментный шейдер в большинстве случаев выполняется только один раз на пиксель**, что гораздо эффективнее чем рендеринг в большем разрешении с последующим уменьшением размеров. 

> Чтобы использовать мультисемплинг необходимо включить соответствующую опцию GPU

Мультисемплинг настраивается через структуру `VkPipelineMultisampleStateCreateInfo`

```c++
VkPipelineMultisampleStateCreateInfo multisampling{};
multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
multisampling.sampleShadingEnable = VK_FALSE;
multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
multisampling.minSampleShading = 1.0f; // Optional
multisampling.pSampleMask = nullptr; // Optional
multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
multisampling.alphaToOneEnable = VK_FALSE; // Optional
```

- `rasterizationSamples` настраивает количество sample  point для каждого пикселя. Значение `VK_SAMPLE_COUNT_1_BIT` соответствует рендерингу без мультисемплинга. Максимальное поддерживаемое GPU число семплов может быть извлечено из `VkPhysicalDeviceProperties` физического устройства. Кроме того устанавливаемое число семплов должно поддерживаться всеми (&) используемыми типами буферов (цвета, глубины, трафарета).

  ```c++
  VkSampleCountFlagBits msaaSamples = VK_SAMPLE_COUNT_1_BIT;
  
  VkSampleCountFlagBits getMaxUsableSampleCount() {
      VkPhysicalDeviceProperties physicalDeviceProperties;
      vkGetPhysicalDeviceProperties(physicalDevice, &physicalDeviceProperties);
      
      VkSampleCountFlags counts = physicalDevicePropeties.limits.framebufferColorSampleCounts 
         & physicalDevicePropeties.limits.framebufferDepthSampleCounts;
      if (VK_SAMPLE_COUNT_64_BIT & counts) {
          return VK_SAMPLE_COUNT_64_BIT;
      }
      if (VK_SAMPLE_COUNT_32_BIT & counts) {
          return VK_SAMPLE_COUNT_32_BIT;
      }
      if (VK_SAMPLE_COUNT_16_BIT & counts) {
          return VK_SAMPLE_COUNT_16_BIT;
      }
      if (VK_SAMPLE_COUNT_8_BIT & counts) {
          return VK_SAMPLE_COUNT_8_BIT;
      }
      if (VK_SAMPLE_COUNT_4_BIT & counts) {
          return VK_SAMPLE_COUNT_4_BIT;
      }
      if (VK_SAMPLE_COUNT_2_BIT & counts) {
          return VK_SAMPLE_COUNT_2_BIT;
      }
      return VK_SAMPLE_COUNT_1_BIT;
  }
  
  void pickPhysicalDevice() {
      ...
      for (const auto& device: devices) {
          if (isDeviceSuitable(device)) {
              physicalDevice = device;
              msaaSamples = getMaxUsableSampleCount();
              break;
          }
      }
      ...
  }
  ```

  

### Тест глубины и тест трафарета

Тест глубины и тест трафарета настраиваются через структуру `VkPipelineDepthStencilStateCreateInfo`

### Смешивание цветов

Цвет, возвращаемый фрагментным шейдером, нужно объединить с цветом, уже находящимся во фреймбуфере. Этот процесс называется смешиванием цветов, и есть 2 способа сделать это:

- смешать старое и новое значение
- объединить старое и новое значение с помощью побитовой операции

Используется 2 типа структур для настройки смешивания:

- `VkPipelineColorBlendAttachmentState` - настройка для каждого подключенного фреймбуфера
- `VkPipelineColorBleandStateCreateInfo` - глобальные настройки смешивания, ссылается на массив структур `VkPipelineColorBlendAttachmentState` для каждого фреймбуфера и позволяет задать константы смешивания, которые можно использовать в качестве коэффициентов смешивания.

Все производимые операции демонстрирует следующий псевдокод:

```c++
if (blendEnable) {
    finalColor.rgb = (srcColorBlendFactor * newColor.rgb) <colorBlendOp> (dstColorBlendFactor * oldColor.rgb);
    finalColor.a (srcAlphaBlendFactor * newColor.a) <alphaBlendOp> (dstAlphaBlendFactor * oldColor.a);
} else {
    finalColor = newColor;
}

finalColor = finalColor & colorWriteMask;
```

Чаще всего для смешивания цветов используют альфа-смешивание, при котором новый цвет смешивается со старым в зависимости от прозрачности.

```c++
finalColor.rbg = newAlpha * newColor + (1 - newAlpha) * oldColor;
finalColor.a = newAlpha.a;
```

Альфа-смешивание настраивается следующим образом:

```c++
VkPipelineColorBlendAttachmentState colorBlendAttachment{};
colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT 
    | VK_COLOR_COMPONENT_G_BIT 
    | VK_COLOR_COMPONENT_B_BIT 
    | VK_COLOR_COMPONENT_A_BIT;
colorBlendAttachment.blendEnable = VK_TRUE;
colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;
```

Все возможные значения и операции перечислены в перечислениях `VkBlendFactor` и `VkBlendOp`

Структура `VkPipelineColorBlendStateCreateInfo` нужна для определения глобального состояния  конвейера, объединяя настройки смешивания, конфигурируемые структурами `VkPipelineColorBlendAttachmentState`, для каждого фреймбуфера.  

```c++
VkPipelineColorBlendStateCreateInfo colorBlending{};
colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
colorBlending.logicOpEnable = VK_FALSE;
colorBlending.logicOp = VK_LOGIC_OP_COPY; // Optional
colorBlending.attachmentCount = 1;
colorBlending.pAttachments = &colorBlendAttachment;
colorBlending.blendConstants[0] = 0.0f; // Optional
colorBlending.blendConstants[1] = 0.0f; // Optional
colorBlending.blendConstants[2] = 0.0f; // Optional
colorBlending.blendConstants[3] = 0.0f; // Optional
```

Для использования побитовых операций **вместо** функции смешивания, нужно установить для поля `logicOpEnable` значение `VK_TRUE`, а в поле `logicOp` указать побитовую операцию. Поле `colorWriteMask` используется и для побитовых операций, чтобы определить содержимое каких каналов будет изменено.

> **При использовании побитовых операций функции смешивания автоматически становятся недоступны**.

Допускается отключение обоих режимов. В этом случае цвета фрагментов будут записаны во фреймбуфер без изменений.

### Динамическое состояние

Некоторые состояния графического конвейера можно изменять не создавая конвейер заново, например размер viewport, ширину отрезков и константы смешивания. Чтобы сделать настройку *динамической* ее необходимо указать при помощи структуры `VkPipelineDynamicStateCreateInfo`. Значения указанных настроек не будут учитываться на этапе создания ковейера. Нужно будет указать их прямо во время отрисовки.

```c++
VkDynamicState dynamicStates[] = {
    VK_DYNAMIC_STATE_VIEWPORT,
    VK_DYNAMIC_STATE_LINE_WIDTH
};

VkPipelineDynamicStateCreateInfo dynamicState{};
dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
dynamicState.dynamicStateCount = 2;
dynamicState.pDynamicStates = dynamicStates;
```

### Layout конвейера

**uniform**-переменные - глобальные переменные шейдера, которые можно изменять динамически для изменения поведения шейдера без его пересоздания.

Эти uniform-переменные необходимо указать во время создания конвейера с помощью структуры `VkPipelineLayout`.

> Экземпляр `VkPipelineLayout` следует сохранить.

Помимо uniform-переменных в структуре также указываются **push**-константы.

```c++
VkPipelineLayoutCreateInfo pipelineLayoutInfo{};
pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
pipelineLayoutInfo.setLayoutCount = 0;
pipelineLayoutInfo.pSetLayouts = nullptr;
pipelineLayoutInfo.pushConstantRangeCount = 0;
pipelineLayoutInfo.pPushConstantRanges = nullptr;

if (vkCreatePipelineLayout(device, &pipelineLayoutInfo, nullptr, &pipelineLayout) != VK_SUCCESS) {
    throw std::runtime_error("failed to create pipeline layout!");
}
```

Экземпляр `VkPipelineLayout` необходимо явно уничтожить

```c++
vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
```

### Проходы рендера (Render passes)

При создании графического конвейера необходимо указать:

- какие буферы (attachments) будут использоваться во время рендеринга 
- сколько будет буферов цвета, глубины, 
- сколько будет семплов для каждого буфера
- как должно обрабатываться содержимое буферов во время рендеринга и т.д. 

Вся эта информация обернута в объект *прохода рендера* (render pass)

#### Подключение буферов к проходу

```c++
void createRenderPass() {
    VkAttachmentDescription colorAttachment{};
    colorAttachment.format = swapChainImageFormat;
    colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    colorAttachment.storeOp = VK_ATTACHMENT_STRE_OP_STORE;
    colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_KHR;
}
```

- `format` - формат цветового буфера должен соответствовать формату image из swap chain
- `sample` - используется для мультисемплинга
- `loadOp` указывает что делать с данными буфера перед рендером
  - `VK_ATTACHMENT_LOAD_OP_LOAD` - буфер буфер будет содержать данные, которые были помещены в него ранее (например во время предыдущего прохода)
  - `VK_ATTACHMENT_LOAD_OP_CLEAR` - буфер очищается в начале прохода рендера
  - `VK_ATTACHMENT_LOAD_OP_DONT_CARE` - содержимое буфера не определено (не имеет значения)
- `storeOp` указывает что делать с данными буфера после рендера
  - `VK_ATTACHMENT_STORE_OP_STORE` - результаты рендера сохраняются для дальнейшего использования
  - `VK_ATTACHMENT_STROE_OP_DONT_CARE`- буфер не используется и его содержимое не имеет значения
- `stencilLoadOp` и `stencilStoreOp` - аналогичные настройки для буфера трафарета
- `initialLayout` и `finalLayout` указывают layout пикселей перед началом прохода и в который image будет автоматически переведен по его завершению

Текстуры и фреймбуферы в Vulkan - это объекты `VkImage` с определенным форматом пикселей. Однако их назначение (layout пикселей в памяти) может быть разным. Далее представлены некоторые из наиболее распространенных layout-ов

- `VK_IMAGE_LAYOUT_UNDEFINED` - неопределенный layout, не гарантирует сохранение image
- `VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIONAL` - images используются в качество цветового буфера
- `VK_IMAGE_LAYOUT_PRESENT_SRC_KHR` - images используются для показа на экране
- `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL` image принимает данные во время операций копирования

#### Подпроходы (subpasses)

Один проход рендера может состоять из множества подпроходов (subpasses). Подпроходы - это последовательные операции рендеринга, зависящие от содержимого фреймбуферов в предыдущих проходах. К ним относятся, например, эффекты постобработки, применяемые друг за другом. 

Каждый подпроход ссылается на один или несколько attachment-ов, указанные в структурах `VkAttachmentReference`. 

```c++
VkAttachmentReference colorAttachmentRef{};
colorAttachmentRef.attachment = 0;
colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
```

- `attachment`  - порядковый номер буфера в массиве `VkAttachmentDescription`, на который ссылается подпроход.
- `layout` указывается layout буфера во время подпрохода. Vulkan автоматически переведет буфер в этот layout когда начнется подпроход.

Сам же подпроход описывается при помощи структуры `VkSubpassDescription`

```c++
VkSubpassDescription subpass{};
subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
subpass.colorAttachmentCount = 1;
subpass.pColorAttachments = &colorAttachmentRef;
subpass.pInputAttachments = nullptr; // optional
subpass.pResolveAttachments = nullptr; // optional
subpass.pDepthStencilAttachment = nullptr; // optional
subpass.pPreserveAttachments = nullptr; // optional
```

- `pipelineBindPoint` - тип подпрохода.

  > Необходимо явно указать, что подпроход является графическим т.к. не исключено что в будущем Vulkan может поддерживать вычислительные подпроходы. 

- `colorAttachmentCount` и `pColorAttachments` устанавливают ссылки на цветовые буферы

  > Директива `layout (location = 0) out vec4 outColor` ссылается именно на порядковый номер буфера в массиве `subpass.pColorAttachments`

  Помимо `pColorAttachments` подпроход может ссылаться на следующие типы буферов:

  - `pInputAttachments` - буферы, содержимое которых читается из шейдера
  - `pResolveAttachments` - буферы, которые используются для цветовых буферов с мультисэмплингом
  - `pDepthStencilAttachment` - буферы глубины и трафарета
  - `pPreserveAttachments` -  буферы, которые не используются в текущем подпроходе, но данные которых должны быть сохранены

#### Проход рендера (Render pass)

Подключенные к проходу буферы и ссылающиеся на эти буферы подпроходы объединяются в структуре `VkRenderPassCreateInfo` необходимой для создания объекта прохода рендера.

```c++
VkRednerPassCreateInfo renderPassInfo{};
renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
renderPassInfo.attachmentCount = 1;
renderPassInfo.pAttachments = &colorAttachment;
renderPassInfo.subpassCount = 1;
renderPassInfo.pSubPasses = &subpass;
if (VK_SUCCESS != vkCreateRenderPass(device, &renderPassInfo, nullptr, &renderPass)) {
    throw std::runtime_error("failed to create render pass!");
}
```

Ссылка на проход рендера понадобится на протяжении всего жизненного цикла программы, а по ее завершению объект прохода рендера необходимо явно уничтожить:

```c++
vkDestroyRenderPass(device, renderPass, nullptr);
```

### Graphics pipeline

Итак для создания графического конвейера необходимо:

- Шейдерные модули, определяющие функционал программируемых стадий конвейера
- Конфигурация непрограммируемых стадий (input assebmbler, растеризатор, viewport и функция смешивания)
- Layout конвейера (описание uniform-переменных и push-констант, используемые конвейером, которые могут обновляться динамически)
- Проход рендера (Render Pass): используемые буферы (attachments), подпроходы

Эта информация объединяется в структуре `VkGraphicsPipelineCreateInfo` для создания объекта графического конвейера.

```c++
VkPipeline graphicsPipeline;

VkGraphicsPipelineCreateInfo pipelineInfo{};
pipeline.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
pipeline.stageCount = 2;
pipeline.pStages = shaderStages;
pipelineInfo.pVertexInputState = &vertexInputInfo;
pipelineInfo.pInputAssemblyState = &inputAssembly;
pipelineInfo.pViewportState = &viewportState;
pipelineInfo.pRasterizationState = &rasterizer;
pipelineInfo.pMultisampleState = &multisampling;
pipelineInfo.pDepthStencilState = nullptr; // Optional
pipelineInfo.pColorBlendState = &colorBlending;
pipelineInfo.pDynamicState = nullptr; // Optional
pipelineInfo.layout = pipelineLayout;
pipelineInfo.renderPass = renderPass;
pipelineInfo.subpass = 0;
pipelineInfo.basePipelineHandle = VK_NULL_HANDLE; // Optional
pipelineInfo.basePipelineIndex = -1; // Optional

if (VK_SUCCESS != vkCreateGraphicsPiplines(device, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &graphicsPipeline)) {
    throw std::runtime_error("failed to create graphics pipeline!");
}
```

- `pipelineInfo.subpass` - номер прохода, используемый в создаваемом конвейере

  > Во время рендера можно использовать и другие объекты прохода вместе с создаваемым конвейером, но они должны быть совместимы. Требования к совместимости указаны [здесь](https://www.khronos.org/registry/vulkan/specs/1.0/html/vkspec.html#renderpass-compatibility)`

- `basePipelineHandle` и `basePipelineIndex` используются для создания производных конвейеров. Данные значения используются только если в поле `flag` указано `VK_PIPELINE_CREATE_DERIVATIVE_BIT` 

  > Vulkan позволяет создать производный графический конвейер из существующего. Создание производного конвейера дешевле т.к. большинство функций берется из родительского конвейера, а переключение между дочерними конвейерами одного родителя осуществляется намного быстрее. 

Функция `vkCreateGraphicsPipelines` позволяет создать несколько объектов `VkPipeline` за один вызов используя информацию из массива структур `VkGraphicsPipelinCreateInfo`.

Необязательный аргумент типа `VkPipelineCashe` - кэш конвейера, используемый для хранения и повторного использования данных, связанных с созданием конвейера.

> Кеш может быть выгружен на диск для переиспользования при последующих запусках программы, что может значительно ускорить процесс создания конвейера. 

Графический конвейер понадобится для всех операций рисования и должен быть явно уничтожен по завершению программы

```c++
vkDestroyPipeline(device, graphicsPipeline, nullptr);
```

## Фреймбуферы

Фреймбуфер (Framebuffer) - коллекция буфферов (memory attachments) с которыми работает проход рендера. 

Каждый swap chain image нужно обернуть в фреймбуфер чтобы использовать данные изображения как цели рендера и использовать тот фреймбуфер, к которому прикреплен интересующий нас image.

Следовательно ссылки фрейбуферы необходимо сохранить и явно уничтожить по завершению программы  **перед image view и render pass** 

```c++
std::vector<VkFramebuffer> swapChainFramebuffers;

void createFrameBuffers() {
    swapChainFramebuffers.resize(swapChainImageViews.size());
    for (size_t i = 0; i < swapChainImageViews.size(); i++) {
        VkImageView attachments[] = {
            swapChainImageViews[i]
        };
        VkFramebufferCreateInfo framebufferInfo{};
        framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebufferInfo.renderPass = renderPass;
        framebufferInfo.attachmentCount = 1;
        framebufferInfo.pAttachments = attachments;
        framebufferInfo.width = swapChainExtent.width;
        framebufferInfo.height = swapChainExtent.height;
        framebufferInfo.layers = 1;
        if (vkCreateFramebuffer(device, &framebufferInfo, nullptr, &swapChainFramebuffers[i])) {
            throw std::runtime_error("failed to create framebuffer!");
        }
    }
}

void cleanup() {
    for (auto framebuffer : swapChainFramebuffers) {
        vkDestroyFramebuffer(device, framebuffer, nullptr);
    }
}
```

При создании фреймбуфера необходимо указать с каким **renderPass** он должен быть совместим. *Фреймбуферы могут быть использованы только с совместимыми проходами рендера (одинаково количество и тип буферов)*

Параметры `attchmentCount` и `pAttachments` указывают на объекты `VkImageView` которые должны соответствовать описанию `pAttachments`, использованному при создании `VkRenderPass`

Поле `layers` указывает количество слоев для images.

## Command buffer

Команды в Vulkan, такие как операции рисования или операции перемещения в памяти, не выполняются в момент вызова. Все необходимые операции должны быть записаны в буфер команд. Благодаря этому сложный процесс настройки команд может быть выполнен заранее и распараллелен в несколько потоков. В потоке отрисовки останется только указать Vulkan какой буфер команд исполнить.

### Пул команд

Пул команд управляет памятью, которая используется для хранения буферов команд и необходим для их создания. Буферы команд будут использоваться на протяжении всего жизненного цикла программы, поэтому пул нужно явно уничтожить в самом конце.

```c++
VkCommandPool commandPool;

void createCommandPool() {
    QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);

	VkCommandPoolCreateInfo poolInfo{};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = queueFamilyIndices.graphicsFamily.value();
	poolInfo.flags = 0; // Optional
    if (vkCreateCommandPool(device, &poolInfo, nullptr, &commandPool) != VK_SUCCESS) {
    	throw std::runtime_error("failed to create command pool!");
	}
}

void cleanup() {
    vkDestroyCommandPool(device, commandPool, nullptr);
}
```

Для создания пула команд потребуется только 2 параметра:

- `queueFamilyIndex` - все буферы команд, выделенные из этого пула должны быть отправлены в очередь указанного семейства.
- `flags` - битовая маска, указывающая как буферы выделенные из данного пула будут использоваться.
  - `VK_COMMAND_POOL_CREATE_TRANSIENT_BIT`- буферы из пула будут часто сбрасываться и выделяться (может поменять поведение пула при выделения памяти)
  - `VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT`- позволяет перезаписывать буферы независимо (если флаг не установлен сбросить буферы можно будет только все и одновременно)

### Буферы команд

Каждая команда рисования устанавливает связь с фреймбуфером. Следовательно необходимо создать буферы для каждого image из swap chain. 

> После уничтожения пула команд, буферы команд автоматически освобождаются.

```c++
std::vector<VkCommandBuffer> commandBuffer;

void createCommandBuffer() {
    commandBuffers.resize(swapChainFramebuffers.size());
    
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
    
    if (vkAllocateCommandBuffers(device, &allocInfo, commandBuffers.data()) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate command buffers!");
    }
}
```

Параметр `level` определяет первичными или вторичными будут создаваемые буферы.

- `VK_COMMAND_BUFFER_LEVEL_PRIMARY` - первичные буферы могут отправляться в очередь, но не могут вызываться из других буферов команд

- `VK_COMMAND_BUFFER_LEVEL_SECONDARY` - вторичные буферы не отправляются в очередь напрямую, но могут вызываться из первичных буферов команд

  > Может быть удобным перенести во вторичные буферы часто используемые последовательности команд и переиспользовать их в первичных.

### Запись буфера команд

Запись буфера команд начинается с вызова `vkBeginCommnadBuffer` в аргументах которого передается структура `VkCommandBufferBeginInfo`, содержащая информацию об использовании буфера. 

```c++
for (size_t i = 0; i < commandBuffers.size(); i++) {
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = 0;
    beginInfo.pInheritanceInfo = nullptr;
    
    if (VK_SUCCESS != vkBeginCommandBuffer(commandBuffers[i], &beginInfo)) {
        throw std::runtime_error("failed to begin recording command buffer!");
    }
}
```

- `flag` - битовая маска, указывающая как будет использоваться буфер команд:
  -  `VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT` после каждого запуска буфер команд перезаписывается
  - `VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT` указывает что это вторичный буфер, который используется в пределах одного прохода рендера
  - `VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT` буфер может быть повторно отправлен в очередь даже если предыдущий его вызов еще не выполнен и находится в состоянии ожидания.
- `pInheritanceInfo` определяет состояние, наследуемое из первичных буферов (использует для вторичных буферов)

> **Нельзя добавлять команды в уже записанный буфер**. Если буфер ранее уже был записан, вызов `vkBeginCommandBuffer` неявно сбросит его.

Начнем проход рендера вызвав `vkCmdBeginRenderPass`. Проход рендера настраивается с помощью некоторых параметров в структуре `VkRenderPassBeginInfo`

```c++
VkRenderPassBeginInfo renderPassInfo{};
renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
renderPassInfo.renderPass = renderPass;
renderPassInfo.framebuffer = swapChainFramebuffers[i];
renderPassInfo.renderArea.offset = {0, 0};
renderPassInfo.renderArea.extent = swapChainExtent;
VkClearValue clearColor = {0.0f, 0.0f, 0.0f, 1.0f};
renderPassInfo.clearValueCount = 1;
renderPassInfo.pClearValues = &clearColor;

vkCmdBeginRenderPass(commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);
```

- `renderArea.offset` и `renderArea.extent` определяют размер области рендера - участок фреймбуфера, на котором шейдеры могут сохранять и загружать данные. Пикселы за пределами этой области будут иметь неопределенные значения.

  > Для лучшей производительности область рендера должна соответствовать размеру фреймбуферов.

- `clearColor` и `pClearValues` определяют значения для очистки буферов при использовании операции `VK_ATTACHMENT_LOAD_OP_CLEAR`

Для каждой команды первым параметром является буфер команд в который данная команда записывается. 

Последний параметр `vkCmdBeginRenderPass` определяет как поступать с командами из вторичного буфера:

- `VK_SUBPASS_CONTENTS_INLINE` команды будут вписаны в первичный буфер без запуска вторичных буферов
- `VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS` команды будут запущены из вторичных буферов

Затем следует подключить графический конвейер:

```c++
vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
```

Здесь 2й параметр указывает какой конвейер используется - графический или вычислительный.

И наконец рисуем треугольник:

```c++
vkCmdDraw(commandBuffers[i], 3, 1, 0, 0);
```

Помимо буфера команд в функцию передаются следующие параметры:

- `vertexCount` - количество вершин
- `instanceCount` - используется когда необходимо отрисовать несколько экземпляров одного объекта (instance rendering)
- `firstVertex` - смещение в вершинном буфере, определяет наименьшее значение `gl_VertexIndex`
- `firstInstance` - смещение при отрисовке нескольких экземпляров одного объекта, определяет наименьшее значение `gl_InstanceIndex`

Осталось завершить проход рендера и запись буфера команд:

```c++
vkCmdEndRenderPass(commandBuffers[i]);
if (VK_SUCCESS != vkEndCommandBuffer(commandBuffers[i])) {
    throw std::runtime_error("failed to record command buffer!");
}
```

## Рендеринг

Непосредственный рендер можно разделить на 3 этапа:

- Получение image из swap chain
- Запуск соответствующего буфера команд для этого image
- Возвращение image в swap chain для вывод на экран

Каждое из этих действий выполняется асинхронно (выполнение функции завершается еще до выполнения операции, а порядок операций не определен).

Есть 2 способа синхронизовать операции: с помощью барьеров (*fences*) или семафоров (*semaphores*). Данные объекты используются для координации зависящих друг от друга операций: пока одна операция выполняется, следующая ожидает сигнала от синхронизатора.

Разница между барьером и семафором в том, что состояние барьера можно получить из программы (например при помощи `vkWaitForFences`), чего не скажешь о семафоров.

> Барьеры предназначены для синхронизации рендеринга с программой, а семафоры для координации операций внутри видеокарты.

К примеру используем 2 семафора: 

- для оповещения о том, что image получен и готов к рендерингу
- для оповещения о том, что рендеринг завершен и результат можно выводить на экран

```c++
VkSemaphore imageAvailableSemaphore;
VkSemaphore renderFinishedSemaphore;

void createSemaphores() {
    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    
    if (VK_SUCCESS != vkCreateSemaphore(device, &semaphoreInfo, nullptr, &imageAvailableSemaphore) ||
       VK_SUCCESS != vkCreateSemaphore(device, &semaphoreInfo, nullptr, &renderFinishedSemaphore)) {
        throw std::runtime_error("failed to create semaphores!");
    }
}

void cleanup() {
    vkDestroySemaphore(device, renderFinishedSemaphore, nullptr);
    vkDestroySemaphore(device, imageAvailableSemaphore, nullptr);
}
```

Семафоры необходимо явно уничтожить по завершению программы. 

> В текущей версии API структура, необходимая для создания семафора, содержит единственное поле `sType`

### Получение image из swap chain

```c++
uint32_t imageIndex;
vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);
```

Функция получения Image из swap chain получает следующие параметры:

1. логическое устройство

2. swap chain, из которого следует получить индекс

3. время ожидания в наносекундах, по истечении которого image станет доступным

   > Использование максимального значение в виде `UINT64_MAX` отключает время ожидания

4. семафор для синхронизации

5. барьер для синхронизации

   > Синхронизаторы переходят в состояние signed когда presentation engine закончит работу с image и то станет доступно для рендеринга в него.

6. переменная для получения индекса image из swap chain доступного для рендеринга. Индекс ссылается на `VkImage` в массиве `swapChainImages`

### Исполнение буфера команд

Отправка буфера команд в очередь и синхронизация настраиваются в структуре `VkSubmitInfo`

```c++
VkSemaphore waitSemaphores[] = {imageAvailableSemaphore};
VkSemaphore signalSemaphores[] = {renderFinishedSemaphore};
VkPipelineStageFlags waitStages[] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

VkSubmitInfo submitInfo{};
submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
submitInfo.waitSemaphoreCount = 1;
submitInfo.pWaitSemaphores = waitSemaphores;
submitInfo.pWaitDstStageMask = waitStages;
submitInfo.commandBufferCount = 1;
submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
submitInfo.signalSemaphoreCount = 1;
submitInfo.pSignalSemaphores = signalSemaphores;
if (VK_SUCCESS != vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE)) {
    throw std::runtime_error("failed to submit draw command buffer!");
}
```

- `submitInfo.waitSemaphoreCount`, `submitInfo.pWaitSemaphores`  - какие семафоры необходимо дождаться перед выполнение указанных этапов графического конвейера. Каждый элемент в массиве `waitStages` соответствует семафору с тем же индексом в `pWaitSemaphores`

  > Ожидание получения изображения на этапе записи в цветовой буфер (`VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT`) означает, что теоретически выполнение вершинного шейдера может начаться раньше, чем станет доступно image.

- `submitInfo.commandBufferCount` и `submitInfo.pCommandBuffers` указывают командные буферы отправляемые для выполнения

  > Необходимо указывать те командные буферы, которые привязаны к соответствующим image

- `submitInfo.signalSemaphoreCount` и `submitInfo.pSignalSemaphores` указывают какие семафоры будут переведены в состояние *signaled* после окончания рендеринга

> Функция `vkQueueSubmit` принимает массив структур `VkSubmitInfo` в качестве аргумента для большей эффективности при большой нагрузке.
>
> Последний параметр `vkQueueSubmit` - опциональный барьер, который переходит в состояние *signaled* при завершении выполнения буферов команд.

### Зависимости подпроходов

Подпроходы в проходах автоматически заботятся о преобразования lyaout изображения. Эти преобразования контролируются *subpas dependencies* которые определяют тип размещения в памяти и зависимости операций подпроходов. Операции непосредственно **до** и сразу **после** подпрохода также считаются неявным "подпроходами".

Существует 2 встроенные зависимости (subpas dependency) которые заботятся о преобразованиях перед начало прохода и после него. Предполагается что преобразование layout изображения **произойдет до начала прохода**, но на данном этапе image еще не получен и преобразование не происходит в нужное время. 

Для решения данной проблемы можно:

- дождаться получение изображение до начала прохода, изменив `waitStages` для `imageAvailableSemaphore` на `VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT`
- заставить проход рендера ждать на `VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT`

Зависимости подпроходов указываются через `VkSubpassDependency`

```c++
VkSubpassDependency dependency{};
dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
dependency.dstSubpass = 0;
dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependency.srcAccessMask = 0;
dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
```

- `srcSubpass` и `dstSubpass` определяют индексы зависимых подпроходов. Значение `VK_SUBPASS_EXTERNAL` указывает на неявный подпроход - начинающий он или завершающий определяется в каком именно из параметров он указан (в `srcSubpass` или `dstSubpass`). 

  > Значение `dstSubpass` всегда должно быть больше чем `srcSubpass`, чтобы не допустить циклов в графе зависимостей (за исключением специального значения `VK_SUBPASS_EXTERNAL`)

- `srcAccessMask` и `srcStageMask` указывают ожидаемый тип доступа к памяти и этапы на котором требуется доступ

- `dstStageMask` и `dstAccessMask` предотвратят передачу данных до тех пор, пока это не станет действительно необходим (и будет разрешено): до момента записи цвета в буфер.

В структуре `VkRenderPassCreateInfo` есть два поля для передачи массива зависимостей:

```c++
renderPassInfo.dependencyCount = 1;
renderPassInfo.pDependencies = &dependecy;
```

### Отображение на экране

Отображение на экране настраивается при помощи `VkPresentInfoKHR`

```c++
VkSwapchainKHR swapChains[] = {swapChain};

VkPresentInfoKHR presentInfo{};
presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
presentInfo.waitSemaphoreCount = 1;
presentInfo.pWaitSemaphores = signalSemaphores;
presentInfo.swapchainCount = 1;
presentInfo.pSwapChains = swapChains;
presentInfo.pImageIndices = &imageIndex;
presentInfo.pResults = nullptr; // optional

vkQueuePresentKHR(presentQueue, &presentInfo);
```

- `waitSemaphoreCount` и `pWaitSemaphores` указывают какие семафоры нужно дождаться перед началом отображения.

- `swapchainCount`, `pSwapChains` и `pImageIndices` указывают swap chains для представления images  и индекс image для каждого указанного swap chain.

  > Почти всегда будет использоваться только один swap chain

- `pResults` опциональный массив значений `VkResult`, в который будут записаны результаты выполнения функции отображения.

  > Данный параметр имеет смысл только при использовании нескольких swap chain. Иначе можно просто использовать возвращаемое значение. 

Функция `vkQueuePresentKHR` отправляет запрос на представление image в swap chain. 

> Ошибки, возвращенные функциями `vkAcquireNextImageKHR` и `vkQueuePresentKHR` необязательно приводят к закрытию программы.

Все операции выполняются асинхронно. Это значит что операции рендеринга и отображения **могут выполняться в момент завершения программы** и освобождать ресурсы в такой момент не самая лучшая идея. 

Для предотвращения ошибок перед уничтожением окна нужно дождаться когда логическое устройство завершит операции.

Самый примитивный способ синхронизации здесь - дождаться завершения операций в очереди логического устройства:

```c++
vkDeviceWaitIdle(device);
```

### Кадры в конвейере

Если просто зациклить отправку командного буфера, то возможно ситуация, когда GPU не будет успевать обрабатывать запросы отрисовки, а семафоры будут переиспользоваться сразу для нескольких кадров. 

Самый простой способ решить проблему - дождаться завершения обработки кадра:

```c++
vkQueuePresentKHR(presentQueue, &presentInfo);
vkQueueWaitIdle(presentQueue);
```

Однако в этом случае **графический конвейер будет использован не оптимально**. Этапы, через которые уже прошел кадр освобождаются и уже могут использоваться для обработки следующего кадра. 

Концептуально ***для каждого кадра должен быть свой набор семафоров***. Кроме того для предотвращения повторной отправки кадра на обработку с тем же индексом необходимо дождаться завершения обработки предыдущего. Для синхронизации CPU-GPU Vulkan предлагает использовать барьеры (fences). 

Тогда необходимо отслеживать какой кадр обрабатывается и использовать соответствующую пару семафоров, а перед отправкой дождаться уведомление об освобождении индекса in flight кадра. Для этого используется закольцованный счетчик.

Кроме того, если `MAX_FRAMES_IN_FLIGHT` больше чем количество images в swap chain или `vkAcquireNextImageKHR` возвращает image не по порядку, есть вероятность, что может начаться рендеринг в image который уже находится в конвейере. Для отслеживания доступности image установим барьер для каждого из swap chain image.

```c++
const int MAX_FRAMES_IN_FLIGHT = 2;

struct InFlightFrameData {
    VkSemaphore imageAvailableSemaphore;
    VkSemaphore renderFinishedSemaphore;
    VkFence fence;
}

size_t currentFrame = 0;
std::vector<InFlightFrameData> inFlightFrameData;
std::vector<VkFence> imageInFlightFences;

void createSyncObjects() {
    inFlightFrameData.resize(MAX_FRAMES_IN_FLIGHT);
    imageInFlightFences.resize(swapChainImages.size(), VK_NULL_HANDLE);
    
    VkSemaphoreCreateInfo semaphoreInfo{};
    semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    VkFenceCreateInfo fenceInfo{};
    fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        if (VK_SUCCESS != vkCreateSemaphore(device, &semaphoreInfo, nullptr, &inFlightFrameData[i].imageAvailableSemaphore) 
            || VK_SUCCESS != vkCreateSemaphore(device, &semaphoreInfo, nullptr, &inFlightFrameData[i].renderFinishedSemaphore)
            || VK_SUCCESS != vkCreateFence(device, &fenceInfo, nullptr, &inFlightFrameData[i].fence) {
            throw std::runtime_error("failed to create synchronization objects for a frame!");
        }
    }
}

void drawFrame() {
    vkWaitForFences(device, 1, &finFlightFrameData[currentFrame].fence, VK_TRUE, UINT64_MAX);
    
    vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, inFlightFrameData[currentFrame].imageAvailableSemaphore, VK_NULL_HANDLE, &imageIndex);
    if (VK_NULL_HANDLE != imagesInFlight[imageIndex]) {
        vkWaitForFences(device, 1, &imageInFlightFences[imageIndex], VK_TRUE, UINT64_MAX);
    }
    ...
    VkSemaphore waitSemaphores[] = {inFlightFrameData[currentFrame].imageAvailableSemaphore};
    ...
    VkSemaphore signalSemaphores[] = {inFlightFrameData[currentFrame].renderFinishedSemaphore};
    ...
    vkResetFences(device, 1, &inFlightFrameData[currentFrame].fence);
    if (VK_SUCCESS != vkQueueSubmit(graphicsQueue, 1, &submitInfo, inFlightFrameData[currentFrame].fence)) {
        throw std::runtime_error("failed to submit draw command buffer!");
    }    
    ...
    currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void cleanup() {
    for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
        vkDestroySemaphore(device, inFlightFrameData[i].imageAvailableSemaphore, nullptr);
        vkDestroySemaphore(device, inFlightFrameData[i].renderFinishedSemaphore, nullptr);
        vkDestroyFence(device, inFlightFrameData[i].fence, nullptr);
    }
}
```

> Флаг `VK_FENCE_CREATE_SIGNALED_BIT`, указанный при создании барьера позволяет инициализировать барьер в состоянии signaled. По умолчанию барьеры создаются в состоянии unsignaled

Функция `vkWaitForFences` принимает массив барьеров и ждет когда один из них или все перейдут в состояние signaled. Значение `VK_TRUE` означает ожидание всех барьеров. Как и функция `vkAcquireNextImageKHR` принимает в качестве параметра время ожидания. 

> В отличие от семафоров нужно вручную сбросить барьер в состояние unsignaled с помощью вызова `vkResetFences`

## Вершинные буферы

Буферы в Vulkan - это участки памяти для хранения произвольных данных, которые могут читаться видеокартой.

### Объявление входных атрибутов в вершинном шейдере

Ключевое слово `in` в вершинном шейдере указывает что данные для атрибута будут получены из вершинного буфера.

```glsl
#version 450
layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 outFragColor;

void main() {
    gl_Position = vec4(inPosition, 0.0, 1.0);
    outFragColor = inColor;
}
```

> Некоторые типы, например 64-битные векторы (`dvec3`) используют несколько слотов `location`. Это означает, что следующий индекс после него должен быть как минимум на 2 больше.

### Привязки (bindings)

Следующий шаг - сообщить Вулкану как передавать данные из буфера в вершинный шейдер.

```c++
struct Vertex {
    glm::vec2 pos;
    glm::vec3 color;
    
    static VkVertexInputBindingDescription getBindingDescription() {
        VkVertexInputBindingDescription bindingDescription{};
        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof(Vertex);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        return bindingDescription;
    }
    
    static std::array<VkVertexInputAttributeDesctiption, 2> getAttributeDescriptions() {
        std::array<VkVertexInputAttributeDescription, 2> attributeDesscriptions{};
        
        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[0].offset = offsetof(Vertex, pos);
        
        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);
        return attributeDescriptions;
    }
}
```

Структура `VkVertexInputBindingDescription` определяет как данные располагаются в памяти.

- `bindingDescription.binding` указывает номер привязки в массиве
- `bindingDescription.stride` расстояние между элементами данных в байтах
- `bindingDescription.inputRate` определяет когда следует переходить к следующей записи:
  - `VK_VERTEX_INPUT_RATE_VERTEX` - после каждой вершины
  - `VK_VERTEX_INPUT_RATE_INSTANCE` - после каждого экземпляра

Структура `VkVertexInputAttributeDescription` описывает как интерпретировать входные данные вершин.

- `attributeDesciptions.binding` сообщает Vulkan от какой привязки поступают данные вершины

- `attributeDescriptions.location` ссылает на директиву `location` вершинного шейдера

- `attributeDescriptions.format` описывает тип данных для атрибут и неявно определяет размер. Обычно используют такие соответствия форматов:

  | Shader type | Format                        |
  | ----------- | ----------------------------- |
  | float       | VK_FORMAT_R32_SFLOAT          |
  | vec2        | VK_FORMAT_R32G32_SFLOAT       |
  | vec3        | VK_FORMAT_R32G32B32_SFLOAT    |
  | vec4        | VK_FORMAT_R32G32B32A32_SFLOAT |

  > Необходимо использовать формат, в котором количество цветовых каналов совпадает с количеством компонентов в типе данных шейдера. Тип цвета (SFLOAT, UINT, SINT) и разрядность также должно соответствовать типу выходных данных шейдера. 
  >
  > Можно использовать больше каналов, чем количество компонентов в атрибуте шейдера, но они будут автоматически отброшены. Если количество каналов меньше количества компонентов атрибута, будут использованы значения по умолчанию.

- `attributeDescriptions.offset` указывает смещение данных атрибута от начала считанного для вершины куска.

### Входные данные конвейера 

Добавим описание входных данных на этапе создания графического конвейера

```c++
void createGrphicsPipeline() {
    ...
    auto bingindDescription = Vertex::getBindingDescription();
    auto attributeDesciptions = Vertex::getAttributeDescription();
    
    vertexInputInfo.vertexBindDescriptionCount = 1;
    vertexInputInfo.vertexAttributeDesriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
    vertexInputInfo.pVertexBindingDescriptions = &bindingDescription;
    vertexInputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();
}
```

### Создание вершинного буфера

В отличие от других объектов Vulkan, буферы не выделяют для себя память автоматически.

Для создания буфера требуется заполнить структуру `VkBufferCreateInfo`

```c++
VkBuffer vertexBuffer;

void createVertexBuffer() {
    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = sizeof(vertices[0] * vertices.size());
    bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    bufferInfo.flags = 0;
    
    if (VK_SUCCESS != vkCreateBuffer(device, &bufferInfo, nullptr, &vertexBuffer)) {
        throw std::runtime_error("failed to create vertex buffer!");
    }
}

void cleanup() {
    vkDestroyBuffer(device, vertexBuffer, nullptr);
}
```

- `bufferInfo.size` - размер буфера в байтах

- `bufferInfo.usage` - для каких целей будут использоваться данные в буфере.

  - `VK_BUFFER_USAGE_VERTEX_BUFFER_BIT` - буфер данных вершин. Может использоваться в команде `vkCmdBindVertexBuffers`
  - `VK_BUFFER_USAGE_INDEX_BUFFER_BIT` - буфе индексов. Может использоваться в команде `vkCmdBindIndexBuffer`
  - `VK_BUFFER_USAGE_TRANSFER_SRC_BIT` - буфер используется в качестве источника во время операции передачи памяти
  - `VK_BUFFER_USAGE_TRANSFER_DST_BIT` - буфер используется в качестве назначения во время операции передачи памяти

  > Для указания нескольких целей использует побитовый оператор `or` 

- `bufferInfo.sharingMode` - также как и image буферы могут использоваться в единственном семействе очередей, либо доступ к ним может быть осуществлен сразу из нескольких очередей разных семейств. 

- `bufferInfo.flags` используется для работы с разреженным (sparse) размещением в памяти.

Буфер должен быть доступен для использования в командах рендеринга вплоть до окончания работы программы. Поэтому его нужно явно уничтожить по ее завершении.

#### Требования к выделенной памяти

Буфер создан, но память для него еще не выделена. 

Перед выделением памяти нужно запросить требования к ней с помощью функции `vkGetBufferMemoryRequirements`

```c++
VkMemoryRequirements memRequirements;
vkGetBufferMemoryRequirements(device, vertexBuffer, &memRequirements)
```

Структура `VkMemoryRequirements` содержит 3 поля:

- `size` - размер необходимого объема памяти в байтах (может отличаться от `bufferInfo.size`)
- `alignment` выравнивание в байтах: *смещение буфера относительно начала выделенного блока должно быть кратно этой величине*. Зависит от `bufferInfo.usage` и `bufferInfo.flags`
- `memoryTypeBits`: битовое поле типов памяти, подходящих для буфера

Ввиду того, что видеокарты предлагают различные типы памяти для выделения, отличающиеся друг от друга разрешенными операциями и производительностью, необходимо определить тип памяти отвечающий требованиям буфера и программы.

```c++
uint32_t findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memProperties);
    for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
        if (typeFilter & (1 << i) 
            && properties == (memProperties.memoryTypes[i].propertyFlags & properties)) {
            return i;
        }
        throw std::runtime_error("failed to find suitable memory type!");
    }
}
```

Получить информацию о доступных типах памяти можно при помощи `vkGetPhysicalDeviceMemoryProperties`. Структура `VkPhysicalDeviceMemoryProperties` содержит 2 массива:

- `memoryTypes` - доступные типы памяти. Состоит из структур `VkMemoryType`, которые указывают кучу и свойства каждого типа памяти. Свойства определяют особенности памяти:

  - `VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT` возможность отображения в память хоста
  - `VK_MEMORY_PROPERTY_HOST_COHERENT_BIT` указывает что команды хоста `vkFlushMappedMemoryRanges` и `vkInvalidateMappedMemoryRanges` не нужны для сброса отображенных буферов

- `memoryHeaps` состоит из структур типа `VkMemoryHeap`, каждая из которых соответствует куче, из которой можно выделить память. Каждому типу памяти соответствует своя куча. 

  > Выбор кучи может существенно влиять на производительность

`typeFilter` указывает битовое поле подходящих типов памяти. Т.е. найти индекс подходящей памяти можно просто перебрав их и проверив установлен ли соответствующий бит (`typeFilter & (1 << i)`). 

Т.к. может быть больше одного требуемого свойства, нужно проверить чтобы результат побитовой операции был равен побитовому полю необходимых свойств `properties == (memProperties.memoryTypes[i].propertyFlags & properties)`

#### Выделение памяти

Для выделения памяти необходимо заполнить структуру `VkMemoryAllocateInfo`

```c++ 
VkDeviceMemory vertexBufferMemory;

void createVertexBuffer() {
    ...
    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size();
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
    
    if (VK_SUCCESS != vkAllocateMemory(device, &allocInfo, nullptr, &vertexBufferMemory)) {
        throw std::runtime_error("failed to allocate vertex buffer memory");
    }
    vkBindBufferMemory(device, vertexBuffer, vertexBufferMemory, 0);
}

void cleanup() {
    ...
    vkDestroyBuffer(device, vertexBuffer, nullptr);
    vkFreeMemory(device, vertexBufferMemory, nullptr):
}
```

Если выделение памяти прошло успешно, можно привязать к ней буфер с помощью `vkBindBufferMemory`, где 4й параметр - смещение в памяти для буфера.

#### Заполнение вершинного буфера

Для заполнения вершинного буфера его память необходимо отобразить в память хоста с помощью `vkMapMemory`, скопировать данные при помощи `memcpy` и отсоединиться от памяти при помощи `vkUnmapMemory`

```c++
void* data;
vkMapMemory(device, vertexBufferMemory, 0, bufferInfo.size(), 0, &data);
	memcpy(data, vertices.data(), (size_t) bufferInfo.size());
vkUnmapMemory(device, vertexBufferMemory);
```

Однако драйвер может не сразу начать копировать данные в буферную память (например, из-за кеширования). 

Также есть вероятность, что данные буфера не сразу будут видны в отображаемой памяти.

Есть 2 способа решить проблему:

- При помощи флага `VK_MEMORY_PROPERTY_HOST_COHERENT_BIT`, для согласования памяти хоста с памятью видеокарты

  > Может привести к снижению производительности

- Вызывать `vkFlushMappedMemoryRanges` *после записи* данных в отображаемую память и `vkInvalidateMapp`

Использование `vkFlushMappedMemoryRanges ` или когерентной памяти (`VK_MEMORY_PROPERTY_HOST_COHERENT_BIT`) означает что драйвер будет знать о том, что мы записали что-то в память. Однако передача данных в GPU - это операция, происходящая в фоновом режиме и [будет завершена при следующем вызове `vkQueueSubmit`][https://www.khronos.org/registry/vulkan/specs/1.0/html/vkspec.html#synchronization-submission-host-writes].

> Помимо отображения участка с заданными смещением и размером, можно отобразить весь выделенный участок указав специальное значение `VK_WHOLE_SIZE`
>
> Предпоследний параметр используется для указания флагов

#### Привязка вершинного буфера

Функция `vkCmdBindVertexBuffers` используется для привязки вершинных буферов и принимает следующие аргументы:

1. командный буфер
2. смещение в массиве привязок
3. количество привязок
4. массив привязываемых буферов
5. смещение буфере данных

```c++
void createCommandBuffers() {
    ...
    vkCmdBindPipeline(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, graphicsPipeline);
    
    VkBuffer vertexBuffers[] = {vertexBuffer};
    VkDeviceSize offsets[] = {0};
    vkCmdBindVertexBffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);
    
    vkCmdDraw(commandBuffers[i], static_cast<uint32_t>(vertices.size()), 1, 0, 0);
}
```

#### Загрузка данных через промежуточный буфер

Тип памяти, позволяющий получить к нему доступ из CPU может оказаться неоптимальным для чтения со стороны видеокарты. Для обозначения наиболее оптимальной памяти используется флаг `VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT`, но *эта память обычно недоступна для CPU*. Доступ к такой памяти осуществляется путем копирования через промежуточный буфер, доступный для CPU.

Команды копирования буфера отправляются в очереди с поддержкой операций передачи (`VK_QUEUE_TRANSFER_BIT`)

> Любое семейство очередей с поддержкой `VK_QUEUE_GRAPHICS_BIT` или `VK_QUEUE_COMPUTE_BIT` автоматически поддерживает операции `VK_QUEUE_TRANSFER_BIT`

Общая функция создания буфера и выделения для него памяти:

```c++
void createBuffer(VkDeviceSize size, VkBufferUsageFlags usage, VkMemoryPropertyFlags properties, VkBuffer& buffer, VkDeviceMemory& bufferMemory) {
    VkBufferCreateInfo bufferInfo{};
    bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    bufferInfo.size = size;
    bufferInfo.usage = usage;
    bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    if (vkCreateBuffer(device, &bufferInfo, nullptr, &buffer) != VK_SUCCESS) {
        throw std::runtime_error("failed to create buffer!");
    }

    VkMemoryRequirements memRequirements;
    vkGetBufferMemoryRequirements(device, buffer, &memRequirements);

    VkMemoryAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocInfo.allocationSize = memRequirements.size;
    allocInfo.memoryTypeIndex = findMemoryType(memRequirements.memoryTypeBits, properties);

    if (vkAllocateMemory(device, &allocInfo, nullptr, &bufferMemory) != VK_SUCCESS) {
        throw std::runtime_error("failed to allocate buffer memory!");
    }

    vkBindBufferMemory(device, buffer, bufferMemory, 0);
}
```

Алгоритм создания локального вершинного буфера устройства и его заполнение через промежуточный буфер, видимый CPU, следующий:

- создать видимый CPU промежуточный буфер с флагом `VK_BUFFER_USAGE_TRANSFER_SRC_BIT`

- отразить промежуточный буфер в память хоста и заполнить его данными

- создать локальный вершинный буфер устройства с флагами `VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT`

- создать временный буфер команд с единственной одноразовой (`VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT`) операцией передачи памяти `vkCmdCopyBuffer`

  > Для таких временных буферов эффективнее использовать отдельный пул команд созданный с флагом `VK_COMMAND_POOL_CREATE_TRANSIENT_BIT`, что позволит применить оптимизации на уровне драйвера
  >
  > В отличие от `vkMapMemory` для операции `vkCmdCopyBuffer` нельзя указать `VK_WHOLE_SIZE`

- Отправить буфер команд с операцией передачи памяти на исполнение и дождаться завершения операции при помощи барьера (`vkWaitForFences`) или подождать пока очередь не освободиться (`vkQueueWaitIdle`)

  > В отличие от команд рисования копирование может быть осуществлено здесь и сейчас
  >
  > Барьер позволяет запланировать несколько передач одновременно, что дает драйверу больше возможностей для оптимизации

- Удалить буфер команд, используемый для операции передачи, промежуточный буфер и выделенную для него память

```c++
void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
	VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;
    
    VlCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
    
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
	
    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    
    VkBufferCopy copyRegion{};
    copyRegion.srcOffset = 0;
    copyRegion.dstOffset = 0;
    copyRegion.size = size;
    
    vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
    
    vkEndCommandBuffer(commandBuffer);
    
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    
    vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphicsQueue);
    
    vkFreeCommadBuffers(device, commandPool, 1, &commandBuffer);
}

void createVertexBuffer() {
    VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();
    
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);
    
    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    	memcpy(data, vertices.data(), bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);
    
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);
    
    copyBuffer(stagingBuffer, vertexBuffer, bufferSize);
    
    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}
```

> В реальной программе не стоит вызывать `vkAllocateMemory` для каждого буфера т.к. это приводит к фрагментации видеопамяти, которая ограничена лимитом физического устройства `maxMemoryAllocationCount`. 
>
> [Разработчики драйверов рекомендуют][https://developer.nvidia.com/vulkan-memory-management] **хранить несколько буферов** (например вершинный и индексный) **в одном `VkBuffer`** и использовать смещение в командах `vkCmdBindVertexBuffers`. Таким образом удобнее кешировать данные поскольку они находятся ближе друг к другу. Кроме того можно повторно использовать один и тот же кусок памяти для нескольких ресурсов, если они не используются для рендера кадра (при условии что эти данные будут обновляться) - такой подход называется *наложением* (*aliasing*) и в некоторых функциях Vulkan есть специальные флаги для этого. 
>
> Чтобы одновременного выделить память для множества объектов, нужно создать кастомный аллокатор, который разобьет одно выделение между множеством разных объектов или использовать библиотеку [VulkanMemoryAllocator][https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator] предоставленную GPUOpen.

## Индексный буфер

Во многих мешах одна вершина может быть общей для нескольких треугольников. Если использовать только вершинный буфер, то такие вершины приходится дублировать, что в среднем приводит к 50%-ной избыточности. Для решения этой проблемы используется *индексный буфер*.

Индексный буфер - это, по сути, массив указателей на данные вершин в вершинном буфере.

![индексный буфер](./img/index_buffer.png) 

```c++
const std::vector<Vertex> vertices = {
    {{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}},
    {{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}},
    {{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}},
    {{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}}
};

const std::vector<uint16_t> indices = {
  	0, 1, 2, 2, 3, 0  
};
```

Для индексов можно использовать типы `uint16_t` и `uint32_t` в зависимости от числа вершин.

Как и данные вершин, индексы должны быть загружены в `VkBuffer` с типом использования `VK_BUFFER_USAGE_INDEX_BUFFER_BIT`

```c++
VkBuffer indexBuffer;
VkDeviceMemory indexBufferMemory;

void createIndexBuffer() {
    VkDeviceSize bufferSize = sizeof(uint16_t) * indices.size();

    VkBuffer stagingBuffer;
    VkDeviceMemory stagingBufferMemory;
    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

    void* data;
    vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
    memcpy(data, indices.data(), (size_t) bufferSize);
    vkUnmapMemory(device, stagingBufferMemory);

    createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

    copyBuffer(stagingBuffer, indexBuffer, bufferSize);

    vkDestroyBuffer(device, stagingBuffer, nullptr);
    vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void cleanup() {
    ...
    vkDestroyBuffer(device, indexBuffer, nullptr);
    vkFreeMemory(device, indexBufferMemory, nullptr);
}
```

Для использования индексного буфера его необходимо привязать при помощи команды `vkCmdBindIndexBuffer`, принимающей в качестве аргументов:

1. командный буфер

2. привязываемый индексный буфер

3. смещение начала внутри индексного буфера в байтах

4. тип данных для индексов 

   | C++ type | Vulkan type          |
   | -------- | -------------------- |
   | uint16_t | VK_INDEX_TYPE_UINT16 |
   | uint32_t | VK_INDEX_TYPE_UINT32 |

Вместо команды `vkCmdDraw` следует использовать команду `vkCmdDrawIndexed` со следующими аргментами:

1. командный буфер
2. количество индексов
3. количество экземпляров (инстансинг)
4. смещение в индексном буфере (отсчет от 0)
5. смещение, добавляемое к индексам
6. смещение для инстансинга

```c++
void createCommandBuffers() {
    ...
    vkCmdBindVertexBuffers(commandBuffers[i], 0, 1, vertexBuffers, offsets);
	vkCmdBindIndexBuffer(commandBuffers[i], indexBuffer, 0, VK_INDEX_TYPE_UINT16);
    vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
}
```

> В отличие от вершинных буферов индексный может быть только один и нет никакого способа указывать разные индексы для разных атрибутов вершин.

## Матричные преобразования

### Однородные координаты

Однородные координаты - координаты, обладающие тем свойством, что определяемый ими объект не меняется при умножении всех координат на одно и то же число.

В общем случае однородным представлением n-мерного объекта называют его представление в (n+1)-мерном пространстве путем добавления скалярного множителя (w). Так для координаты (x, y, z) ее однородной координатой будет (x, y, z, w), где:

- w == 1, то (x, y, z, 1) - это позиция в пространстве
- w == 0, то (x, y, z, 0) - это направление.

> При однородных координатах перенос вектора даст тот же самый вектор, а перенос точки - новое положение в пространстве

Однородные координаты используются главным образом для операций переноса на вектор и определения бесконечно удаленной точки. Т.к. преобразования переноса не являются линейными, используют *аффиные преобразования* - такие преобразования, которые можно представить в виде композиции линейного преобразования и переноса, т.е. записать в виде:
$$
f(x) = Ax + b
$$
где А - некоторая матрица, b - некоторый вектор

**Трансформированная вершина является результатом умножения матрицы на саму вершину**
$$
\begin{bmatrix}
a & b & c & d \\
e & f & g & h \\
i & j & k & l \\
m & n & o & p
\end{bmatrix}
*
\begin{bmatrix}
x \\
y \\
z \\
w
\end{bmatrix}
= 
\begin{bmatrix}
ax + by + cz + dw \\
ex + fy + gz + hw \\
ix + jy + kz + lw \\
mx + ny + oz + pw
\end{bmatrix}
$$

### Единичная матрица

$$
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
$$

При умножении единичной матрицы на вектор, результатом будет исходный вектор:
$$
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
*
\begin{bmatrix}
2 \\
3 \\
4 \\
1
\end{bmatrix}
= 
\begin{bmatrix}
1 * 2 + 0 * 3 + 0 * 4 + 0 * 1 \\
0 * 2 + 1 * 3 + 0 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 1 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 0 * 4 + 1 * 1
\end{bmatrix}
=
\begin{bmatrix}
2 + 0 + 0 + 0 \\
0 + 3 + 0 + 0 \\
0 + 0 + 4 + 0 \\
0 + 0 + 0 + 1 
\end{bmatrix}
=
\begin{bmatrix}
2 \\
3 \\
4 \\
1
\end{bmatrix}
$$

```c++
glm::mat4 identityMatrix = glm::mat4(1.0f); 
```

### Матрица переноса

$$
\begin{bmatrix}
1 & 0 & 0 & X \\
0 & 1 & 0 & Y \\
0 & 0 & 1 & Z \\
0 & 0 & 0 & 1
\end{bmatrix}
$$

где (X, Y, Z) - вектор переноса

Таким образом если координата в пространстве (2, 3, 4, 1), а вектор переноса (5, 0, 0), то
$$
\begin{bmatrix}
1 & 0 & 0 & 5 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
*
\begin{bmatrix}
2 \\
3 \\
4 \\
1
\end{bmatrix}
= 
\begin{bmatrix}
1 * 2 + 0 * 3 + 0 * 4 + 5 * 1 \\
0 * 2 + 1 * 3 + 0 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 1 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 0 * 4 + 1 * 1
\end{bmatrix}
=
\begin{bmatrix}
2 + 0 + 0 + 5 \\
0 + 3 + 0 + 0 \\
0 + 0 + 4 + 0 \\
0 + 0 + 0 + 1 
\end{bmatrix}
=
\begin{bmatrix}
7 \\
3 \\
4 \\
1
\end{bmatrix}
$$
Если w == 0, то:
$$
\begin{bmatrix}
1 & 0 & 0 & 5 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
*
\begin{bmatrix}
2 \\
3 \\
4 \\
0
\end{bmatrix}
= 
\begin{bmatrix}
1 * 2 + 0 * 3 + 0 * 4 + 5 * 0 \\
0 * 2 + 1 * 3 + 0 * 4 + 0 * 0 \\
0 * 2 + 0 * 3 + 1 * 4 + 0 * 0 \\
0 * 2 + 0 * 3 + 0 * 4 + 1 * 0
\end{bmatrix}
=
\begin{bmatrix}
2 + 0 + 0 + 0 \\
0 + 3 + 0 + 0 \\
0 + 0 + 4 + 0 \\
0 + 0 + 0 + 0 
\end{bmatrix}
=
\begin{bmatrix}
2 \\
3 \\
4 \\
0
\end{bmatrix}
$$

```c++
glm::mat4 TransaltionMatrix = glm::translate(5.0f, 0.0f, 0.0f);
```

### Матрица масштабирования

$$
\begin{bmatrix}
X & 0 & 0 & 0 \\
0 & Y & 0 & 0 \\
0 & 0 & Z & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
$$

Где X, Y, Z - коэффициенты масштабирование по осям x, y, z соответственно
$$
\begin{bmatrix}
5 & 0 & 0 & 0 \\
0 & 6 & 0 & 0 \\
0 & 0 & 7 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
*
\begin{bmatrix}
2 \\
3 \\
4 \\
0
\end{bmatrix}
= 
\begin{bmatrix}
5 * 2 + 0 * 3 + 0 * 4 + 0 * 1 \\
0 * 2 + 6 * 3 + 0 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 7 * 4 + 0 * 1 \\
0 * 2 + 0 * 3 + 0 * 4 + 1 * 1
\end{bmatrix}
=
\begin{bmatrix}
10 + 0 + 0 + 0 \\
0 + 18 + 0 + 0 \\
0 + 0 + 28 + 0 \\
0 + 0 + 0 + 1 
\end{bmatrix}
=
\begin{bmatrix}
10 \\
18 \\
28 \\
1
\end{bmatrix}
$$

```c++
glm::mat4 ScaleMatrix = glm::scale(5.0f, 6.0f ,7.0f);
```

### Матрица поворота

Любое вращение в трехмерном пространстве может быть представлено как композиция поворотов вокруг 3х ортогональных осей, которой соответствует матрица, равная произведению соответствующих трех матриц поворота

Вращение вокруг оси X:
$$
Tx =
\begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & cosA & -sinA & 0 \\
0 & sinA & cosA & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
$$
Вращение вокруг оси Y:
$$
Ty =
\begin{bmatrix}
cosB & 0 & sinB & 0 \\
0 & 1 & 0 & 0 \\
-sinB & 0 & cosB & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
$$
Вращение вокруг оси Z:
$$
Tz =
\begin{bmatrix}
cosC & -sinC & 0 & 0 \\
sinC & cosC & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
$$
Общая матрица поворота:
$$
Txyz = Tx * Ty * Tz
$$

```c++
glm::mat4 View = glm::rotate(
    glm::mat4(1.0f), // входящая матрица, которая будет перемножена с матрицей поворота
    Rotate.x, // угол поворота
    glm::vec3(0.0f, 1.0f, 0.0f) // номализованный вектор, указывающий оси вращения
);
```

### Model matrix

Итоговая модельная матрица выглядит следующим образом:
$$
ModelMatrix = TransaltionMatrix * RotationMatrix * ScaleMatrix
$$
**Сначала выполняется масштабирование, затем поворот и в самую последнюю очередь перенос**

После применения модельной матрицы координаты вершин объекта переходят из пространства объекта (вершины заданы относительно центра объекта), в мировое пространство (вершины заданы относительно центра мира)

### View matrix

> Движок не перемещает корабль. Корабль остается на месте, а движок перемещает вселенную относительно него.

Видовая матрица (view matrix) переносит объекты в мировом пространстве относительно положения камеры используя те же матричные преобразования: масштабирование, поворот, перенос. 

В библиотеке GLM есть функция для создания видовой матрицы: 

```c++
glm::mat4 viewMatrix = glm::LookAt(
    cameraPosition, // Позиция камеры в мировом пространстве
    cameraTarget,   // Указывает куда вы смотрите в мировом пространстве
    upVector        // Вектор, указывающий направление вверх. Обычно (0, 1, 0)
);
```

После применения видовой матрицы вершины находятся в пространстве координат камеры

### Projection Matrix

На последнем этапе вершины из пространства координат камеры необходимо перевести в *пространство отсечения* (все вершины находятся в небольшом кубе. Все что находится внутри куба выводится на экран)

При отображении объекта огромную роль играет дистанция до камеры (z). Для двух вершин с одинаковыми координатами (x, y), вершина, имеющая большее значение координаты z будет отображаться ближе. Это называется *перспективной проекцией*.

![perspective matrix before](./img/perspective_matrix_before.png)

![perspective matrix after](./img/perspective_matrix_after.png)

Матрица проекции в GLM создается при помощи функции:

```c++
glm::mat4 projectionMatrix = glm::perspective(
    glm::radians(FoV), // Вертикальное поле зрения в радианах. Обычно между 90&deg; (очень широкое) и 30&deg; (узкое)
    4.0f / 3.0f,       // Отношение сторон. Зависит от размеров вашего окна. Заметьте, что 4/3 == 800/600 == 1280/960
    0.1f,              // Ближняя плоскость отсечения. Должна быть больше 0.
    100.0f             // Дальняя плоскость отсечения.
);
```

> Значение w координат в пространстве отсечения может быть не равен 1, что приведет к делению при преобразовании в конечные нормализованные экранные координаты устройства. Это используется в перспективной проекции в качестве перспективного деления и необходимо, чтобы более 
>
> Помимо матрицы перспективной проекции можно использовать матрицу ортогональной проекции для создания которой используется функция:
>
> ```c++
> glm::mat4 projectionMatrix = glm::ortho(left, right, bottom, top, zNear, zFar);
> ```
>
> При ортогональной проекции значения координат z не влияют на размеры объектов. Использует главным образом в 2D графике.
>
> ![ortho vs perspective projection](./img/ortho_vs_perspective.png)

### Model-View-Projection matrix

Итоговая MVP (Model-View-Projection) матрица получается путем перемножения матриц модели, вида и проекции.

***Перемножение матриц осуществляется в обратном порядке.***
$$
MVP = Mprojection * Mview * Mmodel
$$
![MVP semantic](./img/mvp_semantic.png)

Координаты, записанные вершинным шейдером в выходной параметр `gl_Position` должны находится в пространстве отсечения. Перевод координат из пространства координат объекта в пространство координат отсечения осуществляется одним матричным преобразованием путем умножения MVP матрицы на однородные координаты объекта: 
$$
gl_Position = Vclip = MVP * Vlocal
$$

## Descriptor layout 

*Дескрипторы ресурсов* предоставляют шейдерам доступ к буферам и images. Использование дескрипторов состоит из 3-х этапов:

- Указать layout дескриптора на этапе создания конвейера
- Выделить набор дескрипторов из пула дескрипторов
- Привязать (bind) набор дескрипторов во время рендеринга 

*Layout* дескриптора определяет типы ресурсов, к которым будет осуществляться доступ из конвейера (аналогично как render pass определяет типы attachment-ов с которыми он работает)

*Сет дескрипторов* определяет какие именно буферы или images нужно привязать к дескрипторам (аналогично как фреймбуфер определяет, какие именно images использовать для работы)

```c++
struct UniformMvpMat {
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
}
```

```glsl
// Vertex shader
layout(binding = 0) uniform UniformMvpMat {
    mat4 model;
    mat4 view;
    mat4 proj;
} u_mvpMat;

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;

layout(location = 0) out vec3 outColor;

void main() {
    gl_Position = u_mvpMat.proj * u_mvpMat.view * u_mvpMat.model * vec4(inPosition, 0.0, 1.0);
    outColor = inColor;
}
```

Директива `binding` аналогична директиве `location` для атрибутов. 

> Порядок объявления `uniform`, `in` и `out` не имеет значения. 

Сперва необходимо предоставить информацию о каждом дескрипторе, используемом в шейдерах при помощи `VkDescriptorSetLayoutBinding`

```c++
VkDescriptorSetLayout descriptorSetLayout;

void createDescriptorSetLayout() {
    VkDescriptorSetLayoutBinding uboLayoutBinding{};
    uboLayoutBinding.binding = 0;
    uboLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uboLayoutBinding.descriptorCount = 1;
    uboLayoutBinding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    uboLayoutBinding.pImmutableSamplers = nullptr; // Optional
    
    VkDescriptorSetLayoutCreateInfo layoutInfo{};
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	layoutInfo.bindingCount = 1;
	layoutInfo.pBindings = &uboLayoutBinding;
    
    if (vkCreateDescriptorSetLayout(device, &layoutInfo, nullptr, &descriptorSetLayout) != VK_SUCCESS) {
   		throw std::runtime_error("failed to create descriptor set layout!");
    }
}

void createPipelint() {
    ...
    pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutInfo.setLayoutCount = 1;
    pipelineLayoutInfo.pSetLayouts = &descriptorSetLayout;
    ...
}

void cleanup() {
    cleanupSwapChain();
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
}
```

- `uboLayoutBinding.binding` ссылается на значение `layout(binding = 0)` шейдера

- `descriptorType` - тип дескриптора

  - `VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER` - uniform буфер

- `descriptorCount` - количество дескрипторов в привязке, доступ к которым осуществляется в шейдере через uniform-массив

  > Можно использовать для указания трансформации для каждой кости скелета в скелетной анимации.

- `stageFlags` указывает на каких этапах шейдера упоминается дескриптор. В данном поле может быть указана комбинация [VkShaderStageFlagBits][https://www.khronos.org/registry/vulkan/specs/1.2-extensions/man/html/VkShaderStageFlagBits.html] или значение `VK_SHADER_STAGE_ALL_GRAPHICS` 

- `pImmutableSamplers` используется для дескрипторов, связанных с чтением из текстур. 

Все привязки дескрипторов объединены в один объект `VkDescriptorSetLayout`

Layout'ы дескрипторов указываются в Layout'е конвейера.

> Layout дескрипторов должен быть доступен до тех пор, пока мы могут создаваться графические конвейеры. Т.е. до завершения программы.

### Требования к выравниванию

Vulkan ожидает, что данные в структуре будут выровнены в памяти определенным образом:

- Скаляры должны быть выровнены по N (= 4 байта или 32-битное число с плавающей запятой)
- `vec2` должен быть выровнен по 2N (= 8 байт)
- `vec3` или `vec4` должны быть выровнены по 4N (= 16 байтов)
- Матрица `mat4` должна иметь такое же выравнивание как и `vec4`

Так например в структуре:

```c++
struct MVP {
	glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
}
```

Смещение матриц равны 0, 64, 128 соответственно и все они кратны 16.

Но если структура будет следующей: 

```c++
struct Uniform {
    glm::vec2 foo;
    glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
}
```

Смещения элементов станут соответственно 0, 8, 72, 136 и ни одна из матриц не будет удовлетворять условиям выравнивания. Чтобы решить эту проблему можно использовать спецификатор `alignas` (С++ 11)

```c++
struct Uniform {
    glm::vec2 foo;
    alignas(16) glm::mat4 model;
    glm::mat4 view;
    glm::mat4 proj;
}
```

> Добавления дефайна `GLM_FORCE_DEFAULT_ALIGNED_GENTYPES` прямо перед GLM заставляет библиотеку использовать версию `vec2` и `mat4` с уже заданными требованиями к выравниванию. 
>
> ```c++
> #define GLM_FORCE_DEFAULT_ALIGNED_GENTYPES
> #include <glm/glm.hpp>
> ```
>
> Однако данный метод может не работать с вложенными структурами. Поэтому выравнивания **лучше прописывать явно**.

### Uniform-буфер

> Т.к. данные MVP матриц в буфере будут обновляться каждый кадр, локальный буфер устройства лишь снизит производительность.

В ситуациях когда сразу несколько кадров могут обрабатываться одновременно (один рендерится, другой подготавливается к рендерингу) недопустима ситуация когда данные буфера считываются и обновляются процессором одновременно. 

Для решения этой проблемы можно использовать индивидуальный uniform-буфер на для каждого swap chain image.

```c++
std::vector<VkBuffer> uniformBuffers;
std::vector<VkDeviceMemory> uniformBuffersMemory;

void createUniformBuffers() {
    VkDeviceSize bufferSize = sizeof(UniformMvpMat);
    
    uniformBuffers.resize(swapChainImages.size());
    uniformBuffersMemory.resize(swapChainImages.size());
    
    for (size_t i = 0; i < swapChainImages.size(); i++) {
        createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers[i], uniformBuffersMemory[i]);
    }
}

void recreateSwapChain() {
    createFramebuffers();
    createUniformBuffers();
    createCommandBuffers();
}

void cleanupSwapChain() {
    for (size_t i = 0; i < swapChainImages.size(); i++) {
        vkDestroyBuffer(device, uniformBuffers[i], nullptr);
        vkFreeMemory(device, uniformBuffersMemory[i], nullptr);
    }
}
```

Обновление uniform-буфера:

```c++
void drawFrame() {
    uint32_t imageIndex;
    VkResult result = vkAcquireNextImageKHR(device, swapChain, UINT64_MAX, imageAvailableSemaphores[currentFrame], VK_NULL_HANDLE, &imageIndex);
    ...
    updateUniformBuffer(imageIndex);
    ...
}

void updateUniformBuffer(uint32_t currentImage) {
    ...
}
```

> Изначально GLM была разработана для OpenGL, где ось Y в пространстве отсечения имеет противоположное направление.  Поэтому, если не инвертировать ось Y в матрице проекции изображение будет рендериться вверх-ногами. 
>
> ```c++
> mvpMat.proj[1][1] *= -1;
> ```
>
> Кроме того при инвертировании оси Y следует изменить порядок обхода вершин на порядок "против часовой стрелки" вместо "по часовой". Иначе треугольники будут отбраковываться на стадии кулинга (backface culling)
>
> ```c++
> rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
> rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
> ```

## Пул дескрипторов

Сеты дескрипторов не могут быть созданы напрямую. Аналогично командным буферам, их необходимо выделить из пула.

```c++
VkDescriptorPool descriptorPool;

void createDescriptorPool() {
    VkDescriptorPoolSize poolSize{};
    poolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSize.descriptorCount = static_cast<uint32_t>(swapChainImages.size());
    
    VkDescriptorPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = 1;
    poolInfo.pPoolSizes = &poolSize;
    poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());
	poolInfo.flags = 0;
    
    if (vkCreateDescriptorPool(device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
    	throw std::runtime_error("failed to create descriptor pool!");
    }
}

void cleanupSwapChain() {
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
}

void recreateSwapChain() {
    createUniformBuffers();
    createDescriptorPool();
    createCommandBuffers();
}
```

- `poolInfo.maxSets` - максимальное количество сетов дескрипторов, которое можно выделить
- `poolInfo.flags`: можно заполнить значением `VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT`, которое позволит освобождать сеты по отдельности

### Сеты дескрипторов

Создание сетов дескрипторов описывается с помощью структуры `VkDescriptorSetAllocateInfo`

```c++
std::vector<VkDescriptorSet> descriptorSets;

void createDescriptorSets() {
    std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
    VkDescriptorSetAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocInfo.descriptorPool = descriptorPool;
    allocInfo.descriptorSetCount = static_cast<uint32_t>(swapChainImages.size());
    allocInfo.pSetLayouts = layouts.data();
    
    descriptorSets.resize(swapChainImages.size());
    if (vkAllocateDescriptorSets(device, &allocInfo, descriptorSets.data()) != VK_SUCCESS) {
    	throw std::runtime_error("failed to allocate descriptor sets!");
	}
}
```

- `allocInfo.pSetLayouts` - массив layout'ов, по одному на каждый создаваемый сет

  > Для создания нескольких сетов с одним layout необходимо скопировать хендл layout для каждого создаваемого сета:
  >
  > ```c++
  > std::vector<VkDescriptorSetLayout> layouts(swapChainImages.size(), descriptorSetLayout);
  > ```

Аналогично командным буферам в явном уничтожении сетов дескрипторов потребности нет - они будут уничтожены вместе с уничтожением пула дескрипторов.

Дескрипторы, которые ссылаются на буферы, нуждаются в настройке при помощи структуры `VkDescriptorBufferInfo`

```c++
for (size_t i = 0; i < swapChainImages.size(); i++) {
    VkDescriptorBufferInfo bufferInfo{};
    bufferInfo.buffer = uniformBuffers[i];
    bufferInfo.offset = 0;
    bufferInfo.range = sizeof(UniformBufferObject);
    
    VkWriteDescriptorSet descriptorWrite{};
    descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    descriptorWrite.dstSet = descriptorSets[i];
    descriptorWrite.dstBinding = 0;
    descriptorWrite.dstArrayElement = 0;
    descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	descriptorWrite.descriptorCount = 1;
    descriptorWrite.pBufferInfo = &bufferInfo;
    descriptorWrite.pImageInfo = nullptr; // Optional
    descriptorWrite.pTexelBufferView = nullptr; // Optional
    
    vkUpdateDescriptorSets(device, 1, &descriptorWrite, 0, nullptr);
}
```

- `descriptorWrite.dstSet` - сет дескрипторов, который нужно обновить
- `descriptorWrite.dstBinding` - ссылается на привязку шейдера `layout(binding = 0)`
- `descriptorWrite.dstArrayElement` индекс первого элемента, с которого надо начать обновление
- `descriptorWrite.descriptorCount` указывает сколько дескрипторов в массиве, начиная с индекса `descriptorWrite.dstArrayElement` нужно обновить.
- `descriptorWrite.pBufferInfo` используется для дескрипторов, которые ссылаются на буферы
- `descriptorWrite.pImageInfo` используется для дескрипторов, которые ссылаются на image
- `descriptorWrite.pTexelBufferView` используется для дескрипторов, которые ссылаются на buffer views

Настройки дескриптоов обновляются с помощью функции `vkUpdateDescriptorSets`.

> Если буфер полностью перезаписывается в поле `bufferInfo.range` можно указать `VK_WHOLE_SIZE`. 

### Использование дескрипторов

Для привязывание дескрипторов используется команда `vkCmdBindDescriptorSets` 

```c++
vkCmdBindDescriptorSets(commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, 0, 1, &descriptorSets[i], 0, nullptr);
vkCmdDrawIndexed(commandBuffers[i], static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
```

> Допускается привязывать несколько сетов дескрипторов одновременно. Для этого при создании layout'a конвейера необходимо указать несколько layout'ов дескрипторов (по одному на кажды сет). Шейдеры могу ссылаться на определенный сет следующим образом:
>
> ```glsl
> layout(set = 0, binding = 0) uniform ubo {};
> ```
>
> Таким образом можно разделить сет на общие для всех объектов и специфичные для каждого. Это позволит избежать повторной привязки, что потенциально более эффективно.

## Текстуры

Для использования текстур необходимо выполнить следующие шаги:

1. Создать Image объект использующий память устройства

2. Заполнить пикселы созданного объекта из файла изображения

3. Cоздать image sampler 
4. Использовать цвет полученный из дескриптора image sampler для фрагмента

Создание объектов swapchain image автоматизировано расширением. Создание Image объектов и заполнение их данным похоже на создание вершинных буферов через промежуточный. 

>  Хотя возможно создать промежуточный Image объект, Vulkan API позволяет копировать пиксели из промежуточного `VkBuffer`, *что на некоторых устойствах осуществляется быстрее*.

Images могут иметь разные *layouts*, которые влияют на организацию памяти. Так, к примеру, построчное хранение изображений может быть неоптимально для операций рендера.

| Layout                                   | Назначение                                                   |
| ---------------------------------------- | ------------------------------------------------------------ |
| VK_IMAGE_LAYOUT_PRESENT_SRC_KHR          | Оптимально для представления изображения на экране монитора  |
| VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL | Оптимально для записи результатов фрегментного шейдера       |
| VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL     | Оптимально как источник операций перемещения памяти, таких как `vkCmdCopyImageToBuffer` |
| VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL     | Оптимально как приемник в операциях перемещения памяти, таких как `vkCmdCopyBufferToImage` |
| VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL | Оптимально для семплирования из шейдера                      |

Pipeline barrier - один из наиболе распространенных способов переноса layout изображения. Pipeline barrier'ы в основном используются для синхронизации доступа к ресурсам, например, чтобы быть уверенным что запись в изображение окончена до чтения из него.

> Барьеры так же могут быть использованы для перемещения queue family с `VK_SHARING_MODE_EXCLUSIVE` владением. 

## Texture Image

```c++
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

void createTextureImage(const void* a_pixels, 
                        uint32_t a_width,
                        uint32_t a_height,
                        VkFormat a_format,
                        VkImageTiling a_tiling,
                        VkImageUsageFlags a_usage,
                        VkMemoryPropertyFlags a_properties,
                        VkImage& out_textureImage, 
                        VkDeviceMemory& out_textureMemory) {
    
    VkDeviceSize l_imageSize = a_width * a_height * 4;
    
    VkBuffer l_stagingBuffer;
    VkDeviceMemory l_stagingBufferMemory;
    
    createBuffer(l_imageSize, 
                 VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
                 VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 
                 l_stagingBuffer,
                 l_stagingBufferMemory);
    
    void* l_bufferData;
    vkMapMemory(device, l_stagingBufferMemory, 0, l_imageSize, 0, &l_bufferData);
		memcpy(l_bufferData, a_pixels, static_cast<size_t>(l_imageSize));
	vkUnmapMemory(device, l_stagingBufferMemory);
        
    VkImageCreateInfo l_imageInfo{};
    l_imageInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    l_imageInfo.imageType = VK_IMAGE_TYPE_2D;
    l_imageInfo.extent.width = static_cast<uint32_t>(a_width);
    l_imageInfo.extent.height = static_cast<uint32_t>(a_height);
    l_imageInfo.extent.depth = 1;
    l_imageInfo.mipLevels = 1;
    l_imageInfo.arrayLayers = 1;
    l_imageInfo.format = a_format;
    l_imageInfo.tiling = a_tiling;
    l_imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    l_imageInfo.usage = a_usage;
    l_imageInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    l_imageInfo.samples = VK_SAMPLE_COUNT_1_BIT;
    l_imageInfo.flags = 0; // Optional
    
    if (VK_SUCCESS != vkCreateImage(device, &l_imageInfo, nullptr, &out_textureImage)) {
        throw std::runtime_error("failed to create image!");
    }
    
    VkMemoryRequirements l_memRequirements;
    vkGetImageMemoryRequirements(device, out_textureImage, &l_memRequirements);
    
    VkMemoryAllocateInfo l_allocInfo{};
    l_allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    l_allocInfo.allocationSize = memRequirements.size;
    l_allocInfo.memoryTypeIndex = findMemoryType(l_memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);
    
    if (VK_SUCCESS != vkAllocateMemory(device, &l_allocInfo, nullptr, &out_textureMemory)) {
        throw std::runtime_error("failed to allocate image memory!");
        vkDestroyImage(device, out_textureImage, nullptr);
    }
    
    vkBindImageMemory(device, out_textureImage, out_textureMemory, 0);
    
    transitionImageLayout(out_textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
    copyBufferToImage(l_stagingBuffer, out_textureImage, 
                      static_cast<uint32_t>(a_width), static_cast<uint32_t>(a_height));
    transitionImageLayout(out_textureImage, VK_FORMAT_R8G8B8A8_SRGB,
                         VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                         VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    
    vkDestroyBuffer(device, l_stagingBuffer, nullptr);
    vkFreeMemory(device, l_stagingBufferMemory, nullptr);
}

int texWidth, texHeight, texChannels;
stbi_uc* pixels = stbi_load("textures/texture.jpg", &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
if (!pixels) {
    throw std::runtime_error("failed to load texture image!");
}

VkImage textureImage;
VkDeviceMemory textureMemory;

createTextureImage(pixels, 
                   texWidth, 
                   texHeight, 
                   VK_FORMAT_R8G8B8A8_SRGB, 
                   VK_IMAGE_TILING_OPTIMAL, 
                   VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
                   VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, 
                   textureImage, 
                   textureMemory);

stbi_image_free(pixels);
```

По умолчанию заголовок `stb_image.h` определяет только прототипы фукнций. Чтобы вставить реализации необходимо до заголовка добавить дерективу `STB_IMAGE_IMPLEMENTATION`

Значение `STBI_rgb_alpha` заставляет загружать изображения с альфа-каналом, даже если такового на самом деле нет.

Возвращаемое значение - указатель на первый пиксел. Пикселы загружаются построчно, по 4 байта на каждый, поэтому:
$$
ImageSize = texWidth * texHeight * 4
$$

> Пиксели внутри объекта изображения называются **текселями**.

Объекты изображений создаются при помощи функции `vkCreateImage`, в аргументах которой передается структура `VkImageCreateInfo`

- `imageInfo.imageType` определяет какой тип системы координат используется для доступа к данным:
  - `VK_IMAGE_TYPE_1D`: одномерные семплеры используются для доступа к массиву данных или хранения градиента
  - `VK_IMAGE_TYPE_2D`: двумерные семплеры обычно используются для чтения текстур
  - `VK_IMAGE_TYPE_3D`:  трехмерные семплеры используются, например, для хранения воксельных данных

- `imageInfo.extent` определяет размеры изображения (количество текселов на каждой оси). Поэтому значение поля `depth` для двумерной текстуры должно быть 1, а не 0.

- `imageInfo.format` формат изображений

  > Vulkan поддерживает множество форматов для изображений, однако формат текселов в текстуре должен соответствовать формату пикселей в буфере, иначе операция копирования завершится ошибкой.

- `imageInfo.tiling` поддерживает 2 значения:

  - `VK_IMAGE_TILING_LINEAR` текселы расположены в памяти построчно (как в буфере)
  - `VK_IMAGE_TILING_OPTIMAL` текселы расположены в заданом реализацией порядке для оптимального доступа

  > Расположение в памяти нельзя изменить после создания изображения. Поэтому, если необходим прямой доступ к текселам, то следует использовать значение `VK_IMAGE_TILING_LINEAR`

- `imageInfo.initialLayout` поддерживает 2 значения:

  - `VK_IMAGE_LAYOUT_UNDEFINED` - не используется GPU и первый проход удалит текселы
  - `VK_IMAGE_LAYOUT_PREINITIALIZE` - не используется GPU, но первый проход соханит текселы

  > Сохранение текселов может пригодиться, например, в операциях передачи памяти, когда в качестве источника используется изображение.

- `imageInfo.usage` и `imageInfo.sharingMode` аналогичны соответствующим полям при создании буферов

- `imageInfo.flags` есть несколько флагов, которые связаны с *разреженными* изображениями.

  > **Разреженные изображения** - такие изображения, в которых только некоторые регионы действительно хранятся в памяти. Так, например, если 3D текстура используется для воксельной графики, то нет смысла "хранить воздух".

Выделение памяти и привязка изображений осуществляется аналогично с буферами. Так, следует использовать `vkGetImageMemoryRequirements` вместо `vkGetBufferMemoryRequirements`, и `vkBindImageMemory` вместо `vkBindBufferMemory`

Для инициализации изображения посредством буфера необходимо:

1. Перевести layout изображения в `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL`

   > Изображения создается с `VK_IMAGE_LAYOUT_UNDEFINED` layout

2. Скопировать пикселы из буфера в изображения

3. Преобразовать layout изображения в оптимальный для чтения из шейдера (`VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL`)

> Рекомендуется комбинировать эти операции в одном командном буфере и выполнять их асинхронно (особенно преобразования layout'ов и копирование буферов)

## Layout transitions & copy buffer to image

Теперь необходимо переместить данные из буфера в изображение, но для этого изображения должно находиться в правильном layout. Для преобразования layout'а используем `VkImageMemoryBarrier`.

*Image memory barrier* - разновидность pipeline barrier'а, - аналогично *buffer memory barrier*, используется главным образом для синхронизации доступа к изображениям, в частном случае для предотвращения чтения из изображения до завершения операций записи, а также для переноса владельца `VK_SHARING_MODE_EXCLUSIVE` изображений.

```c++
/***
* start onetime command buffer
**/
VkCommandBuffer beginSingleTimeCommands() {
    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandPool = commandPool;
    allocInfo.commandBufferCount = 1;
	
    VkCommandBuffer commandBuffer;
    vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);
    
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    
    vkBeginCommandBuffer(commandBuffer, &beginInfo);
    
    return commandBuffer;
}

/***
** end command buffer and involve it
**/
void endSingleTimeCommands(VkCommandBuffer commandBuffer) {
 	vkEndCommandBuffer(commandBuffer);
    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;
    vkQueueSubmit(graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphicsQueue);
    vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);
}

void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, VkDeviceSize size) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();
    	VkBufferCopy copyRegion{};
    	copyRegion.size = size;
    	vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, 1, &copyRegion);
    endSingleTimeCommands(commandBuffer);
}

void transitionImageLayout(VkImage a_image, VkFormat a_format, VkImageLayout a_oldLayout, VkImageLayout a_newLayout) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();
    	VkImageMemoryBarrier barrier{};
    	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    	barrier.oldLayout = a_oldLayout;
    	barrier.newLayout = a_newLayout;
    	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    	barrier.image = a_image;
    	barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    	barrier.subresourceRange.baseMipLevel = 0;
    	barrier.subresourceRange.levelCount = 1;
    	barrier.subresourceRange.baseArrayLayer = 0;
    	barrier.subresourceRange.layerCount = 1;
    	
    	VkPipelineStageFlags sourceStage;
    	VkPipelineStageFlags destinationStage;
    
    	if (a_oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && a_newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
            barrier.srcAccessMask = 0;
            barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            
            sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
            destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
        } else if (a_oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL && a_newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL) {
            barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            
            sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
            destinationStage = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
        } else {
            throw std::invalid_argument("unsupported layout transition!");
        }
    
    	vkCmdPipelineBarrier(commandBuffer, 
                             sourceStage, destinationStage, 
                             0, 
                             0, nullptr, 
                             0, nullptr, 
                             1, &barrier);
    endSingleTimeCommands(commandBuffer);
}

void copyBufferToImage(VkBuffer a_srcBuffer, VkImage a_dstImage, uint32_t a_width, uint32_t a_height) {
    VkCommandBuffer commandBuffer = beginSingleTimeCommands();
    	VkBufferImageCopy region{};
    	region.bufferOffset = 0;
    	region.bufferRowLength = 0;
    	region.bufferImageHeight = 0;
    	
    	region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    	region.imageSubresource.mipLevel = 0;
    	region.imageSubresource.baseArrayLayer = 0;
    	region.imageSubresource.layerCount = 1;
    	
    	region.imageOffset = {0, 0, 0};
    	region.imageExtent = {a_width, a_height, 1};
    	
    	vkCmdCopyBufferToImage(commandBuffer, a_srcBuffer, a_dstImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    endSingleTimeCommands(commandBuffer);
}
```

- `barrier.oldLayout` и `barrier.newLayout` используются для преобразования layout. 

  > Для поля `barrier.oldLayout` допустимо значение `VK_IMAGE_LAYOUT_UNDEFINED` если существующее изображения не нужно

- `barrier.srcQueueFamilyIndex` и `barrier.dstQueueFamilyIndex` используется для переноса владельца изображения

  > Если перенос не осуществляется обоим полям должно быть присвоено значение `VK_QUEUE_FAMILY_IGNORED`. **Не значение по умолчанию!**

- `barrier.image` и `barrier.subresourceRange` устанавливают затрагиваемую часть изображения 

- `barrier.srcAccessMask` и `barrier.dstAccessMask` определяют какие типы операций должны выполняться до барьера, а какие должны ожидаться. 

Все типы Pipeline Barrier'ов отправляются в очередь при помощи команды `vkCmdPipelineBarrier`, принимающей следующие параметры:

1. командный буфер

2. стадии конвейера операции которых должны выполниться до барьера

3. стадии конвейера, которые ожидают барьера

   > Стадии, которые допустимо определять до и после барьера зависят от того, как используется ресурс до и после. К примеру, если после барьера будет производиться чтение из uniform необходимо указать тип использования `VK_ACCESS_UNIFORM_READ_BIT` и ранний шейдер, который обращается к данной uniform (например `VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT`). 
   >
   > Уровни валидации сообщат о неверном использовании

   В случае инициализации изображения посредством буфера необходимо обработать следующие трансформации layout изображения:

   - `VK_IMAGE_LAYOUT_UNDEFINED` -> `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL`: ничего не ожидать, сразу приступить к записи
   - `VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL` -> `VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL`: фрагментный шейдер перед чтением из текстуры ожидает окончания записи текселов

   > Перемещения данных должны выполняться на псевдо этапе `VK_PIPELINE_STAGE_TRANSFER_BIT`
   >
   > Выполнение командного буфера приводит к неявной `VK_ACCESS_HOST_WRITE_BIT` синхронизации. Поэтому если понадобится данная синхронизация до барьера, значение `srcAccessMask` можно оставить равным 0. Однако, *скрытые операции - плохая практика*.
   >
   > `VK_IMAGE_LAYOUT_GENERAL` поддерживает все возможные операции, но из-за этакого наиболее не оптимален и его следует использовать лишь в случаях крайней необходимости.

4. указывает как формируются зависимости выполнения и памяти. Так? например, флаг `VK_DEPENDENCY_BY_REGION_BIT` переводит барьер в per-region условие (реализация может начать чтение из регионов, которые уже были записаны)

5. Последние 3 пары параметров: memory, buffer memory и image memory барьеры.

Копирование пикселей из буфера в изображение осуществляется командой `vkCmdCopyBufferToImage`.

> Можно скопировать сразу несколько регионов из одного буфера в одно изображение.

### Texture image view

Для изображений текстур, как и для swap chain изображений необходимо создать ImageView обертку.

```c++
VkImageView textureImageView;

VkImageView createImageView(VkImage a_image, VkFormat a_format) {
    VkImageViewCreateInfo viewInfo{};
    viewInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    viewInfo.image = a_image;
    viewInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    viewInfo.format = a_format;
    viewInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    viewInfo.subresourceRange.baseMipLevel = 0;
    viewInfo.subresourceRange.levelCount = 1;
    viewInfo.subresourceRange.baseArrayLayer = 0;
    viewInfo.subresourceRange.layerCount = 1;
    
    VkImageView imageView;
    if (VK_SUCCESS != vkCreateImageView(device, &a_viewInfo, nullptr, &imageView)) {
        throw std::runtime_error("failed to create texture image view!");
    }
    return imageView;
}

void createTextureImageView() {
    textureImageView = createImageView(textureImage, VK_FORMAT_R8G8B8A8_SRGB);
}

void createImageViews() {
    swapChainImageViews.resize(swapchainImages.size());
    for (uint32_t i = 0; i < swapChainImages.size(); i++) {
        swapChainImageViews[i] = createImageView(swapChainImages[i], swapChainImageFormat);
    }
}

void cleanup() {
    vkDestroyImageView(device, textureImageView, nullptr);
}
```

### Samplers

Конечно, шейдеры могут считывать текселы напрямую из изображений, однако чаще изображения используются как текстуры. Доступ к текстурам обычно осуществляется через образцы (samplers) которые применяют фильтрацию и трансформации для вычисления цвета. 

Эти фильтры направлены на решение следующих проблем:

- Передискетизация (oversampling) - ситуация, когда текстура накладывается на геометрию, имеющую больше фрагментов, чем текселей в текстуре. Если не использовать фильтрацию, а просто брать ближайшую координату, получим следующий результат:

  ![oversampling](./img/oversampling.jpg)

  > Bilinear filtering - цвет семпла вычисляется на основе двойной линейной интерполяции 4 ближайших текселей.

- Недостаточная дискретизация (undersampling) - противоположная ситуация, когда в текстуре многим больше текселов, чем фрагментов геометрии, что приведет к появлению артефактов при выборке высокочастотных шаблонов под острым углом:

  ![undersampling](./img/undersampling.jpg)

Кроме того sampler может выполнять преобразования, при попытке чтения текселя за границами изображения (*addressing mode*).

![outside_read_mode](./img/outside_read_mode.jpg)

```c++
VkSampler textureSampler;

void createTextureSampler() {
    VkSamplerCreateInfo samplerInfo{};
    samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    samplerInfo.magFilter = VK_FILTER_LINEAR;
    samplerInfo.minFilter = VK_FILTER_LINEAR;
    samplerInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    samplerInfo.anisotropyEnable = VK_TRUE;
    samplerInfo.maxAnisotropy = 16;
    samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    samplerInfo.unnormalizedCoordinates = VK_FALSE;
    samplerInfo.compareEnable = VK_FALSE;
    samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
    samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    samplerInfo.mipLodBias = 0.0f;
    samplerInfo.minLod = 0.0f;
    samplerInfo.maxLod = 0.0f;
    
    if (VK_SUCCESS != vkCreateSampler(device, &samplerInfo, nullptr, &textureSampler)) {
        throw std::runtime_error("failed to create texture sampler!");
    }
}

void cleanup() {
    vkDestroySampler(device, textureSampler, nullptr);
}
```

Sampler настраивается при помощи структуры `VkSamplerCreateInfo`

- `magFilter` и `minFilter` определяют как интерполировать текселы, при oversampling и undersampling соответственно. Допустимые значения: `VK_FILTER_LINEAR` и `VK_FILTER_NEAREST`

- `adressMode` настраивает преобразования при чтении за границами изображения для каждой оси. Допустимы следующие значения

  - `VK_SAMPLER_ADDRESS_MODE_REPEAT` повторяет текстуру когда текстурные координаты выходят за пределы изображения
  - `VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT` аналогично повторению, только зеркально отражает координаты относительно оси
  - `VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE` использует цвет ближайшего к координате текселя 
  - `VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE` аналогично clamp to edge, только вместо ближайшего к координате текселя использует ему противоположный
  - `VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER` при выборке за пределами границы возвращает сплошной цвет

- `anisotropyEnable` и `maxAnisotropy` определяют следует ли использовать анизотропную фильтрацию. Поле `maxAnisotropy` ограничивает количество образцов текселя, которые используются для расчета. 

  > После 16 образцов разница результатов незначительная
  >
  > Анизатропная фильтрация - опциональная возможность устройства. Не все видеокарты ее поддерживают.
  >
  > ```c++
  > bool isDeviceSuitable(VkPhysicalDevice device) {
  > 	VkPhysicalDeviceFeatures supportedFeatures;
  >     vkGetPhysicalDeviceFeatures(device, &supportedFeatures);
  >     
  >     return indices.isComplete() 
  >         && extensionsSupported 
  >         && swapChainAdequate 
  >         && supportedFeatures.samplerAnisotropy;
  > }
  > ```

- `borderColor` определяет какой цвет будет возвращен при использовании режим `VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER`. Допустимыми значениями является черный, белый и прозрачный в целочисленном формате или в формате с плавающей точкой. Указать произвольный цвет нельзя. 

- `unnormalizedCoordinate` определяет какую систему координат использовать для адресации текселей: ненормализованную ($$U = [0, texWidth]; V = [0, texHeight]$$) или ненормализованную ($$U = [0, 1); V = [0, 1)$$)

  > Использование нормализованной системы позволяет использовать текстуры разного разрешения

- `compareEnable` и `compareOp`: если функция сравнения задана, то текселы сперва сравниваются со значением, а затем результат сравнения используется при фильтрации.

  > В основном это используется для [процентной фильтрации (percentage-closer) карт теней][https://developer.nvidia.com/gpugems/gpugems/part-ii-lighting-and-shadows/chapter-11-shadow-map-antialiasing]

- `mipmapMode`, `mipLodBias`, `minLod`, `maxLod` используются совместно с мип-картами

> Объекты VkSampler нигде не ссылаются на конкретные изображения. Их можно применить с любыми объектами `VkImage`

## Combined image sampler

*Combined image sampler* - дескриптор, для доступа к image объектам из шейдера.

```c++
void createDescriptorSetLayout() {
    ... 
    VkDescriptorSetLayoutBinding samplerLayoutBinding{};
    samplerLayoutBinding.binding = 1;
    samplerLayoutBinding.descriptorCount = 1;
    samplerLayoutBinding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    samplerLayoutBinding.pImmutableSamplers = nullptr;
    samplerLayoutBinding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;

    std::array<VkDescriptorSetLayoutBinding, 2> bindings = {uboLayoutBinding, samplerLayoutBinding};
    VkDescriptorSetLayoutCreateInfo layoutInfo;
    layoutInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
    layoutInfo.pBindings = bindings.data();
}

void createDescriptorPool() {
    std::array<VkDescriptorPoolSize, 2> poolSizes{};
    poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    poolSizes[0].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
    
    poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    poolSizes[1].descriptorCount = static_cast<uint32_t>(swapChainImages.size());
    
    VkDescriptorPoolCreateInfo poolInfo{};
    poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
    poolInfo.pPoolSizes = poolSizes.data();
    poolInfo.maxSets = static_cast<uint32_t>(swapChainImages.size());
}

void createDescriptorSets() {
    ... 
    for (size_t i = 0; i < swapChainImages.size(); i++) {
        VkDescriptorBufferInfo bufferInfo{};
        bufferInfo.buffer = uniformBuffer[i];
        bufferInfo.offset = 0;
        bufferInfo.range = sizeof(UniformBufferObject);
        
        VkDescriptorImageInfo imageInfo{};
        imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        imageInfo.imageView = textureImageView;
        imageInfo.sampler = textureSampler;
        
        std::array<VkWriteDescriptorSet, 2> descriptorWrites{};
        
        VkWriteDescriptorSet descriptorWrites{};
        descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[0].dstSet = descriptorSets[i];
        descriptorWrites[0].dstBinding = 0;
        descriptorWrites[0].dstArrayElement = 0;
        descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        descriptorWrites[0].descriptorCount = 1;
        descriptorWrites[0].pBufferInfo = &bufferInfo;
        
        descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        descriptorWrites[1].dstSet = descriptorSets[i];
        descriptorWrites[1].dstBinding = 1;
        descriptorWrites[1].dstArrayElement = 0;
        descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        descriptorWrites[1].descriptorCount = 1;
        descriptorWrites[1].pImageInfo = &imageInfo;
        
        vkUpdateDescriptorSets(device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(), 0, nullptr);
    }
}
```

### Texture coordinate

Добавим к структуре вершин текстурные координаты: 

```c++
struct Vertex {
    glm::vec2 pos;
    glm::vec3 color;
    glm::vec2 texCoord;
    
    static VkVertexInputBindingDescription getBindingDescription() {
        VkVertexInputBindingDescription bindingDescription{};
        bindingDescription.binding = 0;
        bindingDescription.stride = sizeof(Vertex);
        bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        
        return bindingDescription;
    }
    
    static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
        std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions{};
        attributeDescriptions[0].binding = 0;
        attributeDescriptions[0].location = 0;
        attributeDescriptions[0].format = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[0].offset = offsetof(Vertex, pos);
        
        attributeDescriptions[1].binding = 0;
        attributeDescriptions[1].location = 1;
        attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
        attributeDescriptions[1].offset = offsetof(Vertex, color);
        
        attributeDescriptions[2].binding = 0;
        attributeDescriptions[2].location = 2;
        attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
        attributeDescriptions[2].offset = offsetof(Vertex, texCoord);
        return attributeDescriptions;
    }
}

const std::vector<Vertex> vertices = {
	{{-0.5f, -0.5f}, {1.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
	{{0.5f, -0.5f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f}},
	{{0.5f, 0.5f}, {0.0f, 0.0f, 1.0f}, {1.0f, 1.0f}},
	{{-0.5f, 0.5f}, {1.0f, 1.0f, 1.0f}, {0.0f, 1.0f}}
};
```

### Shaders

```glsl
// vertex shader

layout(location = 0) in vec2 inPosition;
layout(location = 1) in vec3 inColor;
layout(location = 2) in vec2 inTexCoord;

layout(location = 0) out vec3 fragColor;
layout(location - 1) out vec2 fragTexCoord;

void main() {
    gl_Position = ubo.proj * ubo.view * ubo.model * vec4(inPosition, 0.0, 1.0);
    fragColor = inColor;
    fragTexCoord = inTexCoord;
}
```

``` glsl
// fragment shader
#version 450
#extension GL_ARB_separate_shader_object : enable

layout(binding = 1) uniform sampler2D texSampler;
    
layout(location = 0) in vec3 fragColor;
layout(location = 1) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

void main() {
    outColor = texture(texSampler, fragTexCoord);
}
```

## Буфер глубины

Фрагменты объектов, определенных позднее в индексном буфере, накладываются поверх фрагментов более ранних объектов. Существует 2 способа решения данной проблемы:

- отсортировать все draw call по глубине и выводить объекты от дальних к ближним - данный метод обычно используется для рендеринга полупрозрачных объектов поскольку прозрачность независящая от порядка - трудно-решаемая задача.
- использовать тест и буфер глубины

Буфер глубины - буфер хранящий значение глубины для каждого пиксела. Для каждого фрагмента, созданного растеризатором сравнивается его значение глубины с уже записанным значением в буфере. Если новый фрагмент находится ближе предыдущего, значение его глубины записывается в буфер глубины. Фрагменты находящиеся дальше отбрасываются. 

>  Значением буфера глубины можно управлять в фрагментном шейдере

> Матрица проекции, генерируемая GLM по умолчанию использует диапазон глубины, равный $$[-1.0, 1.0]$$. Для настройки диапазона $$[0.0, 1.0]$$ для соответствия со спецификацией Vulkan API необходимо использовать директиву `GLM_FORCE_DEPTH_ZERO_TO_ONE`
>
> ```c++
> #define GLM_FORCE_RADIANS
> #define GLM_FORCE_DEPTH_ZERO_TO_ONE
> #include <glm/glm.hpp>
> #include <glm/gtc/matrix_transform.hpp>
> ```

### Depth Image

Буфер глубины, как и буфер цвета реализован на основе `VkImage`. Реализации Swap chain не создают буферов глубин. Однако в общем случае достаточно одного буфера глубины т.к. одновременно выполняется одна операция рисования.

```c++
VkImage depthImage;
VkDeviceMemory depthImageMemory;
VkImageView depthImageView;

void init() {
    ...
    createDepthResources();
    createFramebuffers();
}

VkImageView createImageView(VkImage image, VkFormat format, VkImageeAspectFlags aspectFlags) {
    ...
    viewInfo.subresourceRange.aspectMask = aspectFlags;
    ...
}

void createDepthResources() {
    VkFormat depthFormat = findDepthFormat();
    createImage(swapChainExtent.width, swapChainExtent.height, depthFormat, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, depthImage, depthImageMemory);
    depthImageView = createImageView(depthImage, depthFormat, VK_IMAGE_ASPECT_DEPTH_BIT);
    
    transitionImageLayout(depthImage, depthFormat, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL); // optional
}
```

К изображениям, используемым в качестве буфера глубины, применяются следующие ограничения:

- разрешение буфера глубины должно быть равным разрешению изображения, для которого оно используется.

- буфер глубины должен быть расположен локально в памяти устройства

- расположение текселей в памяти должно быть оптимальным (`VK_IMAGE_TILING_OPTIMAL`)

- формат изображения должен содержать компонент `_D??_`

  - `VK_FORMAT_D32_SFLOAT`: 32-bit float для значения глубины
  - `VK_FORMAT_D32_SFLOAT_S8_UINT`: 32-bit signed float для значения глубины and 8 bit для значения трафарета
  - `VK_FORMAT_D24_UNORM_S8_UINT`: 24-bit float для глубины and 8 bit для трафарета

  > Конкретный формат для буфера глубины обычно неважен, т.к. программы не работают с ним напрямую. Он просто должен обладать разумной точностью. Обычно достаточно 24-bit.

  `VK_FORMAT_D32_SFLOAT` наиболее распространен, но хорошая практика - проверить доступные форматы: 
  
  ```c++
  VkFormat findSupportedFormat(const std::vector<VkFormat>& candidates, VkImageTiling tiling, VkFormatFeaturesFlags features) {
      for (VkFormat format : candidates) {
          VkFormatProperties props;
          vkGetPhysicalDeviceFormatProperties(physicalDevice, format, &rops);
          
          if (tiling == VK_IMAGE_TILING_LINEAR && (props.linearTilingFeatures & features) == features) {
              return format;
          } else if (tiling == VK_IMAGE_TILING_OPTIMAL && (props.optimalTilingFeatures & features) == features) {
              return format;
          }
      }
      throws std::runtime_error("failed to find supported format");
  }
  
  VkFormat findDepthFormat() {
      return findSupportedFormat({VK_FORMAT_D32_SFLOAT, VK_FORMAT_D32_SFLOAT_S8_UINT, VK_FORMAT_D24_UNORM_S8_UINT}, 
                                VK_IMAGE_TILING_OPTIMAL, 
                                VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT);
  }
  
  bool hasStencilComponent(VkFormat format) {
      return format == VK_FORMAT_D32_SFLOAT_S8_UINT || format == VK_FORMAT_D24_UNORM_S8_UINT;
  }
  ```
  
  Множество допустимых форматов определяется их назначением (usage) и размещением в памяти (tiling). 
  
  `VkFormatProperties` содержит 3 поля:
  
  - liniearTilingFeatures - варианты использования при линейном размещении в памяти
  - optimalTilingFeatures - варианты использования при размещении на усмотрение реализации
  - bufferFeatures - варианты использования для буферов

> Преобразование layout изображения буфера глубины - необязательная операция, т.к. оно будет неявно выполнено при подготовке к проходу рендера:
>
> ```c++
> void transitionImageLayout(...) {
>     if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
>         barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
>         if (hasStencilComponent(format)) {
>             barrier.subresourceRange.aspectMask |= VK_IMAGE_ASPECT_STENCIL_BIT;
>         }
>     } else {
>         barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;;
>     }
>     ... 
>     if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED && newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
>         barrier.srcAccessMask = 0;
>         barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
>         
>         sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
>         destinationStage = VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
>     }
> }
> ```
>
> - чтение из буфера глубины будет осуществляется для прохождения теста глубины (`VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT`), а запись для сохранения значения глубины визуализируемого фрагмента (`VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT`)
> - чтение происходит на этапе `VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT`
> - запись оканчивается на этапе `VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT`

### Configuration

```c++
void createRenderPass() {
    VkAttachmentDescription depthAttachment{};
    depthAttachment.format = findDepthFormat();
    depthAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depthAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depthAttachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depthAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depthAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depthAttachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    VkAttachmentReference depthAttachmentRef{};
    depthAttachmentRef.attachment = 1;
    depthAttachmentRef.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    VkSubpassDescription subpass{};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
    
    std::array<VkAttachmentDescription, 2> attachments = {colorAttachment, depthAttachment};
    VkRenderPassCreateInfo renderPassInfo{};
    renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderPassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    renderPassInfo.pAttachments = attachments.data();
    renderPassInfo.subpassCount = 1;
    renderPassInfo.pSubpasses = &subpass;
    renderPassInfo.dependencyCount = 1;
    renderPassInfo.pDependencies = &dependency;
}

void createFramebuffers() {
    std::array<VkImageView, 2> attachments = {swapChainImageViews[i], depthImageView};
    VkFramebufferCreateInfo framebufferInfo{};
    framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferInfo.renderPass = renderPass;
    framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
    framebufferInfo.pAttachments = attachments.data();
    framebufferInfo.width = swapChainExtent.width;
    framebufferInfo.height = swapChainExtent.height;
    framebufferInfo.layers = 1;
}

void createCommanBuffers() {
    std::array<VkClearValue, 2> clearValues{};
    clearValues[0].color = {0.0f, 0.0f, 0.0f, 1.0f};
    clearValues[1].depthStencil = {1.0f, 0};
    
    renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
    renderPassInfo.pClearValues = clearValues.datat();
}

void createGraphicsPipeline() {
    VkPipelineDepthStencilStateCreateInfo depthStencil{};
    depthStencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencil.deptTestEnable = VK_TRUE;
    depthStencil.depthWriteEnable = VK_TRUE;
    depthStencil.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencil.depthBoundsTestEnable = VK_FALSE;
    depthStencil.minDepthBounds = 0.0f;
    depthStencil.maxDepthBounds = 1.0f;
    depthStencil.stencilTestEnable = VK_FALSE;
    depthStencil.front{};
    depthStencil.back{};
    
   	pipelineInfo.pDepthStencilState = $depthStencil;
}
```

- `depthAttachment.format` должен соответствовать формату изображения буфера глубины
- в отличие от цветовых буферов буфер глубины для каждого подпрохода может быть только один
- т.к. теперь есть 2 подключения использующих `VK_ATTACHMENT_LOAD_OP_CLEAR` необходимо определить значения `VkClearValue`. Диапазон значения буфера глубины в Vulkan API определен как $$[0.0, 1.0]$$ Где 1.0 - дальняя плоскость отсечения, а 0.0 - ближняя плоскость отсечения. Значения `clearValues` должны идти в соответствии с порядком подключений буферов. 
- Тест глубины настраивается при помощи `VkPipelineDepthStencilStateCreateInfo` и должен быть явно указан при наличии подключений буферов глубины или трафарета. `depthTestEnable` включает тест глубины, а `depthWriteEnable` включает перезапись значений в буфере глубины значениями глубин новых фрагментов прошедших тест (полезно при рендеринге прозрачных объектов).
-  `depthStencil.depthCompareOp` - определяет предикат прохождения теста глубины
- триплет `depthBoundsTestEnable`, `minDepthBounds`, `maxDepthBounds` позволяет настраивать диапазон для теста глубины
- `stencilTestEnable`, `front` и `back` относятся к тесту трафарета
- Т.к. разрешение буфера глубины зависит от разрешения swap chain изображения, при изменении разрешения окна буфер глубины необходимо пересоздать.

